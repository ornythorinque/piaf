# Changelog

## 0.3.0-beta.1
### Features
* #45 WebAPI as a behavior
* #43 AMQP 0.9.1 extension

### Bugs
* #46 Agents can't put in WAITING state any other agent

### Others
* #42 Redis extension in a dedicated package
* #41 Move mailboxes from agents to ACC
* #40 Replace dependency aioredis by redis

## 0.2.1
### Bugs
* #37 Missing semver dependency
* #38 Agents can only see service-only extensions

### Others
* Switched to pytest-asyncio to automatically handle asynchrounous tests

## 0.2.0
### Features
* Add py.typed to comply with PEP 561

### Others
* Improved documentation

## 0.2.0-beta.3
### Features
* Read .env file to configure the WebAPI

### Others
* Upgrade dependencies
* Reorganize the WebAPI code
* Document endpoints
* Deploy piaf package in Gitlab Package Registry

## 0.2.0-beta.2
### Features
* Suspend / resume agent using the WebAPI
* Delete an agent using the WebAPI
* Make events available to WebAPI users
* Memory snapshot of an agent using the WebAPI
* Send messages through the WebAPI

### Bugs
* Correct wrong deletion routes

## 0.2.0-beta.1
### Features
* Add the new EventAPI
* Add the WebAPI
* Rename module 'platform' into 'ptf'

### Deprecations
* Module 'platform' is deprecated and will be removed in release 1.0.0

## 0.1
### Features
* Add the SuicideBehavior
* Add the Launch API

### Removals
- Drop Python 3.6 support

### Others
- Use the new poetry installer in CI jobs

## 0.1b1
### Bugfixes
* Update dependencies
* AMQP documentation not generated
* Give a chance to other tasks to run

### Features
* New DF Service (aka yellow pages)
* AMQP MTP

### Others
* Updated documentation
* Moved pytest configuration in pyproject file
* New examples

## 0.1a3
### Bugfixes
* Fixed various bugs in tests
* KeyError when starting then stopping the platform quickly
* IllegalState error when stopping the platform with paused agents

## 0.1a2
### Features
* Agent Communication Channel (ACC)
* Message Transport Protocol (MTP, only interface)
* Messages
* Received Object
* Envelope
* AMS (limited to search function)

### Removals
* Old implementation of the Message Transport System
* Old AMS

### Bugfixes
* Improved testing suite
* Updated dependencies
* Fixed various bugs

### Others
* Updated documentation
* Improved CI process

## 0.1a1
* First project's release