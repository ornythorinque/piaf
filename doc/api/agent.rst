.. currentmodule:: piaf.agent

piaf.agent
==========

.. automodule:: piaf.agent
    :no-members:
    
    .. autoclass:: AgentState(enum.Enum)

    .. autoclass:: Agent()

