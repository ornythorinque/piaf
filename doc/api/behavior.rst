.. currentmodule:: piaf.behavior

piaf.behavior
=============

.. automodule:: piaf.behavior
    :no-members:

    .. autoclass:: Behavior()

    .. autoclass:: CyclicBehavior(Behavior)

    .. autoclass:: FSMBehavior(Behavior)

    .. autoclass:: SuicideBehavior(Behavior)