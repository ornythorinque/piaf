.. currentmodule:: piaf.comm

piaf.comm
=============

.. automodule:: piaf.comm
    :no-members:

    .. autoclass:: AID()

    .. autoclass:: ACLMessage()

    .. autoclass:: Performative(enum.Enum)

    .. autoclass:: MessageTemplate()

    .. autoclass:: MT_OR(MessageTemplate)
    
    .. autoclass:: MT_AND(MessageTemplate)
    
    .. autoclass:: MT_ALL(MessageTemplate)
    
    .. autodata:: MT_PERFORMATIVE(MessageTemplate)
    
    .. autodata:: MT_SENDER(MessageTemplate)
    
    .. autodata:: MT_CONVERSATION_ID(MessageTemplate)
    
    .. autodata:: MT_ENCODING(MessageTemplate)
    
    .. autodata:: MT_IN_REPLY_TO(MessageTemplate)
    
    .. autodata:: MT_LANGUAGE(MessageTemplate)
    
    .. autodata:: MT_ONTOLOGY(MessageTemplate)
    
    .. autodata:: MT_PROTOCOL(MessageTemplate)
    
    .. autodata:: MT_REPLY_WITH(MessageTemplate)
    
    .. autodata:: MT_REPLY_TO(MessageTemplate)

    .. autoclass:: Envelope()

    .. autoclass:: ReceivedObject()

    .. autoclass:: Message()
