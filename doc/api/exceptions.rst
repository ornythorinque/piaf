.. currentmodule:: piaf.exceptions

piaf.exceptions
===============

.. automodule:: piaf.exceptions
    :no-members:

    .. autoexception:: InvalidStateError(Exception)

    .. autoexception:: StateTransitionError(Exception)

    .. autoexception:: UnsupportedOperationError(Exception)

    .. autoexception:: MessageNotSentError(Exception)

    .. autoexception:: MandatoryFieldValueError(Exception)

    .. autoexception:: DuplicatedNameError(Exception)

    .. autoexception:: DuplicatedSchemeError(Exception)

    .. autoexception:: IllegalArgumentError(Exception)
    