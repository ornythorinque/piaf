.. currentmodule:: piaf.launcher

piaf.launcher
=============

.. automodule:: piaf.launcher
    :no-members:
    
    .. autoclass:: AgentDescription()

    .. autoclass:: ServiceDescription()

    .. autoclass:: PlatformLauncher()
