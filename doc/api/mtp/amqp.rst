.. currentmodule:: piaf.comm.mtp.amqp

piaf.comm.mtp.amqp
==================

.. automodule:: piaf.comm.mtp.amqp
    :no-members:

    .. autoclass:: AMQPMessageTransportProtocol(MessageTransportProtocol)