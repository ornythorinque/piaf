.. currentmodule:: piaf.comm.mtp.nats

piaf.comm.mtp.nats
==================

.. automodule:: piaf.comm.mtp.nats
    :no-members:

    .. autoclass:: NATSMessageTransportProtocol(MessageTransportProtocol)