.. currentmodule:: piaf.comm.mts

piaf.comm.mts
=============

.. automodule:: piaf.comm.mts
    :no-members:

    .. autoclass:: PlayloadParser()

    .. autoclass:: MessageTransportProtocol()

    .. autoclass:: AgentCommunicationChannel()

    .. autoclass:: MailBox()

    .. autoclass:: MessageContext()