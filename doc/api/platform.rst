.. currentmodule:: piaf.ptf

piaf.ptf
==================

.. automodule:: piaf.ptf
    :no-members:
    
    .. autoclass:: AgentManager()

    .. autoclass:: AgentPlatformFacade()

    .. autoclass:: PlatformState(enum.Enum)

    .. autoclass:: AgentPlatform()

    .. autoclass:: Extension()