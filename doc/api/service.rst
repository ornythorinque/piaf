.. currentmodule:: piaf.service

piaf.service
============

.. automodule:: piaf.service
    :no-members:

    .. autoclass:: AMSService(Agent)

    .. autoclass:: AMSAgentDescription()

    .. autoclass:: AgentPlatformService()

    .. autoclass:: AgentPlatformDescription()

    .. autoclass:: Property()

    .. autoclass:: AgentCreationDescription()

    .. autoclass:: DFService(Agent)

    .. autoclass:: ServiceDescription()

    .. autoclass:: DFAgentDescription()

    .. autoclass:: SearchConstraints()
    