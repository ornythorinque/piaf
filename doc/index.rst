.. piaf documentation master file, created by
   sphinx-quickstart on Mon Apr 20 19:27:30 2020.

################################
Welcome to piaf's documentation!
################################

**Python Intelligent Agent Framework (piaf)** is a framework that eases multi-agent systems development. It is fully written in modern Python
and uses asynchronous programming behind the woods to power agents.

The aim of **piaf** is to provide a fully FIPA-compliant framework, extensible and lightweight. We also want to modernize the FIPA specification by providing modern technologies like JSON messages, AMQP protocol, and a REST API to interact with platforms.

.. warning:: As this work is only done in our free time, all planned features are not fully implemented.

This documentation is both a user and a technical guide. The first path will teach you how to use the **piaf** library to create your applications, whereas the technical part is more about listing all types and functions the library contains and how they work.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   pages/installation
   pages/quickstart
   pages/user_guide/index
   pages/advanced_guide/index

   api/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
