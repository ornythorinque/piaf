Asynchronicity
==============
Asynchronicity is a key concept in **piaf**.

Communication between agents is a key concept in multi-agent systems. In piaf, they are asynchronous. It means that we are not sure when an agent will decide to respond to the message. Thus agent can't actively wait for others to respond.

Each agent has a mailbox it can consult. When an agent sends a message to another agent, the platform will put the message in the mailbox. Then, it is up to the receiver to open its mailbox and handle messages.

Communication is not the only key concept that is shipped with asynchronicity. Each agent is an autonomous entity, meaning that it has its memory and set of actions, but also it runs on its own. In multi-agent systems, all agents are running concurrently. That is why we often need coordination protocols to realize complex actions.