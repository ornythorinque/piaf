Communication
=============

In multi-agent systems, agents often need to communicate with each other so we need communication support. The additional difficulty, agents are running at their speed, asynchronously.

**piaf** is tackling this by using well-known (at least by scientists) `FIPA`_ specification. This specification defines:

- Message representation, including fields and accepted types
- Envelope enclosing messages
- How to handle envelopes and messages
- Routing
- Communication protocols

Even if the specification is a bit old, it is easier to follow it than reinvent the wheel.

ACLMessage
----------
According to specification [1]_, ACLMessages have two objectives:

1. To help ensure interoperability by providing a standard set of ACL message structure
2. To provide a well-defined process for maintaining this set.

An ACLMessage is composed of a set of fields. Each field has a special meaning: sender, receiver, content ... To help the agent understand the meaning of the message, each ACLMessage must have a **performative**. This field is mandatory and its content should be one of the available communication acts [2]_.

It is worth noticing that the specification splits the abstract representation and the concrete one.

In **piaf**, ACLMessages can be manipulated using the :class:`piaf.comm.ACLMessage` class. Rather than direct instantiation, uses the provided :class:`piaf.comm.ACLMessage.Builder` builder to create your ACLMessages. This will ensure your message is well-formed and respects FIPA specifications.

Here is a small example::

    from datetime import datetime, timedelta
    from piaf.comm import ACLMessage, Performative


    msg = (
        ACLMessage.Builder()
        .performative(Performative.ACCEPT)
        .protocol("fipa.request")
        .conversation_id("some_id")
        .reply_by(datetime.now() + timedelta(minutes=10))
        .build()
    )

Envelope
--------
Once you wrote your ACLMessage, you will send it to one or many receivers. Sometimes the receiver is in the same platform but it might be an agent running in another foreign agent platform. So we need message routing.

Envelopes are a wrapper around ACLMessages and contain crucial information for the routing mechanism. Each time the Envelope is processed by an ACC (Agent Communication Channel), its parameters are updated (but never erased). Thus you can "track" the path taken by the message.

.. note:: For now, you can't specify a message envelope when sending an ACLMessage. It is computed automatically by the **piaf** platform.

When receiving a message, an agent has access to the envelope. There is information that might interest the agent such as encryption, ACLMessage representation, or user-defined fields.

.. note:: Notice that the method :meth:`piaf.agent.Agent.receive` will return a :class:`Message` instance. This class is just a named tuple that contains both the Envelope and the ACLMessage.

Message routing
---------------
Message routing describes how messages are forwarded by the ACC (Agent Communication Channel). Routing is based on information carried by the message envelope.

Message recipients are specified in the `to` parameter. It contains a list of AIDs (Agent IDentifier). If an ACC receives a message envelope without the `intended-receiver` field filled, then it uses the `to` parameter to fill it. If the `to` parameter contains multiple AIDs, the ACC can decide to split AIDs list into several parts.

.. note:: In piaf, we build two lists: the first one contains local agents and the second one foreign agents. Then the message is forwarded in case of local agents or replicated and sent to each foreign agent (one copy per agent).

If an ACC receives a message envelope with the `intended-receiver` field filled, then it uses it to forward the message.

Once there is only one receiver specified in the `intended-receiver` field:

- The ACC tries to deliver the message using the first address specified in the transport address list of the AID.
- If it fails (because the agent or the platform is not available, the MTP is not supported ...) then the ACC creates a new `intended-receiver` entry containing the AID without the tried address. Then it starts over.
- If delivery is still unsuccessful, the ACC may try to resolve the AID using the resolvers field.
- Finally, if all failed, an error message is passed back to the sending agent.

.. warning:: **piaf** doesn't support name resolution yet.

Error messages
--------------
If an error message needs to be returned, the message generated:

- Has a communicative act set to `failure`
- Contains the ACL Message that was not delivered
- Has a `predicate-symbol` set to `internal-error`

Other information can be included.

.. warning:: For now, the implemented ACC will fail silently. No error messages are sent to the sender.


.. Links definition
.. _FIPA: http://fipa.org/index.html

.. Citations
.. [1] FIPA ACL Message Structure Specification, http://fipa.org/specs/fipa00061/SC00061G.html
.. [2] FIPA Communicative Act Library Specification, http://fipa.org/specs/fipa00037/SC00037J.html
