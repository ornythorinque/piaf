Inter-platform communications
=============================

Making the agents communicate within a platform is good, but what if you could make two agents communicate with each other, even if they are not on the same platform?

The FIPA specification defines a way to connect two (or more) platforms and enable such inter-platform communications [1]_ called **Message Transport Protocol**.

Message Transport Protocol
--------------------------

Each FIPA platform can use one or several MTPs. Each MTP enables a specific message transportation protocol like IIOP [2]_, HTTP [3]_, or any other protocol. To connect two platforms, you have to make sure that both have at least one MTP in common.

For example, if platform A has HTTP MTP and platform B has IIOP MTP, then they can't communicate with each other.

.. warning:: The piaf platform can only handle binary object transport using the AMQP or the NATS protocol, meaning that you can't connect a piaf platform with another implementation of the FIPA specification (like Jade [4]_ for example). Although this is something we want, it requires intermediary developments like the message and envelope XML serialization and FIPA SL.

From the user perspective, you don't need to know how each MTP implementation works but rather what is the platform address. With piaf, this can be obtained by reading the :attr:`piaf.comm.mts.MessageTransportProtocol.address` property. Once the platform's address is known, each agent AID will have the following form::

    AID("agent_name@hap_name", addresses=["mtp_hap_address"])

When communicating with a foreign agent, remember that you have to include in its AID one of the foreign platform's addresses (and one that the current platform supports).

AMQP in piaf
------------

To run this example, you will have to install:

    1. Extra dependencies. They can be installed using poetry (``poetry install -E "amqp-mtp"``) or pip (``pip install piaf["amqp-mtp"]``)
    2. An AMQP 0-9-1 server. We recommend `RabbitMQ <https://www.rabbitmq.com/>`_ but any server should work. If you have docker installed, you can use the following to start a ready-to-use RabbitMQ server: ``docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.9-management-alpine``

The full source code is available here: https://gitlab.com/ornythorinque/piaf/-/tree/master/src/piaf/examples/two_platforms. In this example, we connect two piaf instances using the :class:`AMQPMessageTransportProtocol`.
On the first platform, an agent sends a message to another agent, running inside the second platform. The latter displays the received message and dies.


.. Citations
.. [1] FIPA Agent Message Transport Service Specification, http://fipa.org/specs/fipa00067/SC00067F.html
.. [2] FIPA Agent Message Transport Protocol for IIOP Specification, http://fipa.org/specs/fipa00075/SC00075G.html
.. [3] FIPA Agent Message Transport Protocol for HTTP Specification, http://fipa.org/specs/fipa00084/SC00084F.html
.. [4] JADE Website, https://jade-project.gitlab.io/
