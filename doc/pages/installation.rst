Installation guide
==================

**Piaf** can be installed like any other python library. This page shows two different technics using either **pip** tool or **poetry**. 

.. important:: In both cases, the minimum required Python version is 3.7 which can be downloaded `here`_.


Using pip
---------
**pip** is a widely-used tool to fetch and install python libraries stored in the `PYthon Package Index (PYPI)`_.

First, ensure that **pip** is installed and available in your terminal: ``pip --version``.

.. note:: On linux, **pip** sometimes refers to Python2.X pip. To use Python3.X pip, type ``pip3 --version`` instead

If **pip** is not installed, follow the instructions on the project's `page`_.

Once **pip** is up and ready, installing **piaf** is simple-easy:

1. Open a terminal
2. Type ``pip install piaf``
3.  If the installation is successful, you can start developing your own agents

Using poetry
------------
**poetry** is quite new but very powerful. Like **pip**, it can fetch and install python libraries but also manage virtual environments and has a built-in dependencies resolver.

**poetry** can be installed globally as an independent tool or like any python library using **pip**. Check the `project's page`_ to see how to install **poetry** on your system.

Once **poetry** is installed you can install **piaf**:

1. Use command ``poetry new <your-project-name>`` to create a new project
2. Next, type ``poetry add piaf`` to add **piaf** to your project's dependencies
3. Finally, type ``poetry install`` to download and install **piaf** in your python environment

Installing an MTP implementation
--------------------------------
To enable communication between two independent platforms, you will need to register an MTP (Message Transport Protocol). The default installation doesn't install the dependencies
you may need to execute a given MTP.
For now, piaf is shipped with two MTP implementations:

- :class:`AMQPMessageTransportProtocol`. To install the dependencies it needs, use ``pip install piaf["amqp-mtp"]`` or ``poetry install -E amqp-mtp``
    if you are using poetry.
- :class:`NATSMessageTransportProtocol`, which can be installed with ``pip install piaf["nats-mtp"]`` or ``poetry install -E nats-mtp``


For developers
--------------
**piaf** is using **poetry** to manage its dependencies. Even if you can setup your environment manually, we recommend that you use **poetry**.
It will ensure that all dev environments are identical for all developers.

1. First clone the repository available at `<https://gitlab.com/ornythorinque/piaf>`_
2. Then go to the **piaf** folder and run command ``poetry install``
3. You are set :)

Running tests
^^^^^^^^^^^^^
**piaf** is using `coverage`_ and `pytest`_ packages for its test suite. We configured **coverage** so it can run **pytest**. This ensures you get both test results and coverage each time!

Poetry will take care of virtual environments for you. To run tests, go to the project's root folder and type in you favorite terminal :
``poetry run coverage run``

Reports are generated in the folder ``/.report``. By default, only the **pytest** report is saved. If you want a nice coverage report, type either :

- ``poetry run coverage report`` if you want the ASCII report in your terminal
- ``poetry run coverage xml`` to get an XML coverage report
- ``poetry run coverage html`` to get a readable HTML coverage report

Building wheel and tarball
^^^^^^^^^^^^^^^^^^^^^^^^^^
Again, it is very simple to generate the wheel and the tarball: ``poetry build``.

Wait ... That's it ?? Simple told you!


.. links section
.. _here: https://www.python.org/
.. _PYthon Package Index (PYPI): https://pypi.org/
.. _page: https://pip.pypa.io/en/stable/installing/
.. _project's page: https://python-poetry.org/docs/
.. _pytest: https://docs.pytest.org/en/stable/index.html
.. _coverage: https://coverage.readthedocs.io/en/coverage-5.4/#
