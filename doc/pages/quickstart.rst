Quickstart
==========
This page will teach you a few things to quickly start using **piaf** framework.

.. note:: Check the :doc:`installation page <installation>` first to install **piaf** framework in your python environment.

Writing a Behavior
------------------
All behaviors should extend the base class :class:`Behavior`. There are two methods that you can redefine:

* :meth:`Behavior.action` which is the body of your behaviors
* :meth:`Behavior.done` that stops the behavior when evaluated to ``True``

You can check these methods' API documentation :doc:`here <../api/behavior>`.

Each behavior has access to the calling agent and can thus send messages, update the agent's mind, or do whatever your agent can do.
Here is an example of a behavior using the agent's logger to print ``"Hello World"``:

.. code-block:: python

    from piaf.behavior import Behavior

    class MyCustomBehavior(Behavior):
        """Behavior that displays 'Hello World' in the agent's log."""

        async def action(self):
            self.agent.logger.info("Hello World")

By default, behaviors are one shot. It means that once the action method has been executed, the behavior is destroyed by the agent.
You can change this by redefining the method :meth:`Behavior.done`. Until this method returns ``True``, the :meth:`Behavior.action` method will be executed.

Writing an Agent
----------------
The next step is to write your agent. Each agent class should subclass the :class:`piaf.agent.Agent` class. You can add
as many methods as you want and call them later in a behavior. Adding behavior to an agent is done by calling the method :meth:`Agent.add_behavior`. Once a behavior
is done, it is removed from the agent behavior list.

Here is an example of a customs agent :

.. code-block:: python

    from piaf.agent import Agent

    class AwesomeAgent(Agent):
        """Customized agent with extra methods and initialized with behaviors."""

        def __init__(self, aid, platform):
            super().__init__(aid, platform)

            # You can add here some behaviors
            self.add_behavior(MyCustomBehavior())
            ...
        
        def foo(self, bar):
            """This method can be called in a behavior."""
            pass


Each :class:`Agent` is shipped with some methods to communicate with other agents and also has a unique identifier called
AID (Agent IDentifier). See the API documentation to learn more about agents.

Running the platform
--------------------
Once you have written your behaviors and agents, you will need to start the platform. This platform will execute your agents and act as a communication layer between them.

To ease the configuration and the run of a platform, **piaf** provides a simple API called :class:`PlatformLauncher`. Given a name and a bunch of :class:`AgentDescription`, you will be able to run your platform.

Here is an example with one agent named "hello":

.. code-block:: python
    :linenos:

    import logging
    from piaf.launcher import PlatformLauncher, AgentDescription

    # Create the launcher
    launcher = PlatformLauncher("localhost")

    # Add one agent to the simulation
    launcher.add_agent(AgentDescription("hello", AwesomeAgent)

    # Configure the logger to see things
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(logging.StreamHandler())

    # The program will run until you hit Ctrl+C
    launcher.run()

:class:`PlatformLauncher` provides a method to add services called :meth:`PlatformLauncher.add_service` and can configure the platform to communicate with another
one (inter-platform communications) by adding one or more MTPs using the :meth:`PlatformLauncher.add_mtp` method.

Going further
-------------
Now you know the basics to create agents, and behaviors and run them on a platform. To go further, check out some advanced concepts like :doc:`communication </pages/advanced_guide/communication>` or
take a look at the full :doc:`API <../api/index>` documentation !

We also provide ready-to-use examples on many aspects of the platform like communication between agents, AMS and DF services, and even inter-platform connection.
All those examples are available in the `project's repository <https://gitlab.com/ornythorinque/piaf/-/tree/master/src/piaf/examples>`_.
