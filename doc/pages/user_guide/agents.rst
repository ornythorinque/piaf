Agents
======

Agents are a key component of the multi-agent research field. They intended to be autonomous, making individual decisions based on their representation of the environment.

Agents in piaf are containers holding what we call their "memory" and the list of running behaviors. The memory is the agent's representation of the current environment state, being internal (agent's internal step in its lifecycle, the list of active behaviors, ...) or external (known agents, positions, ...). Behaviors are reasoning processes working with the agent's memory (learn more about them :doc:`here </pages/user_guide/behaviors>`).

Lifecycle
---------

Each agent follows the lifecycle defined in the `FIPA Agent Management Specification`_.

.. figure:: /_static/images/agent_lifecycle.gif
    :alt: Figure of the agent's lifecycle: possible states and transitions between them.
    :align: center

    State machine of an agent's lifecycle.

    ..

.. note:: The ``transit`` state is currently not supported by piaf since agents can't move from one platform to another.

Once an agent is in the ``active`` state, registered behaviors are run asynchronously and in parallel. Switching to another state than ``active`` prevents behaviors to run. In addition, if there are no more behaviors to run, the agent automatically invokes the ``quit`` operation.

Create and invoke an agent
--------------------------

In piaf, there are two ways to create and then invoke an agent:

1. If you have access to the :class:`~piaf.ptf.AgentPlatform` object, you can use the attached :class:`~piaf.ptf.AgentManager` instance.
2. If you are using the :class:`piaf.launcher.PlatformLauncher` interface, you rather supply an agent's description which is used then to create and invoke your agent.
3. If you want to start an agent within another agent, you can ask the AMS to do it using the :attr:`~piaf.service.AMSService.CREATE_AGENT_FUNC` operation.

With the :class:`~piaf.ptf.AgentPlatform` object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To launch an agent using this method, two separate steps are required: creating the agent first and then invoking it. There is also one prerequisite: the platform must be started first.

Here is the minimal code to launch an agent:

.. code-block:: python
    :linenos:

    import asyncio
    from piaf.ptf import AgentPlatform
    from piaf.agent import Agent

    async def main(ptf: AgentPlatform):
        # Prerequisite: the platform is started
        await ptf.start()

        # First, create the agent
        aid = await ptf.agent_manager.create(Agent, "MyAgent")

        # Then invoke
        await ptf.agent_manager.invoke(aid)

    # Manual launch of the platform, a bit verbose.
    try:
        ptf = AgentPlatform("localhost")
        loop = asyncio.get_event_loop()
        loop.create_task(main(ptf))
        loop.run_forever()
    except KeyboardInterupt:
        pass
    finally:
        loop.run_until_complete(ptf.stop())
        loop.run_until_complete(ptf.evt_manager.close())
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()

Let's concentrate on the ``main`` function, which holds the code to launch our agent. First things first, almost everything is coroutines, including the ``main`` function. This is why you can see the ``async`` keyword in line 5 and a bunch of ``await`` ones in lines 7, 10, and 13. We assume you are familiar with asynchronous programming in Python!

Back to the ``main`` function, you can see that we first start the platform line 7: it ensures the AMS service, which is in charge of the agents' management, is started before trying to add agents.

Next (line 10), we create the agent using the :meth:`AgentManager.create() <piaf.ptf.AgentManager.create()>` method. It takes at least two arguments: the agent's class and a name for your agent. If the call is successful, the agent manager attributes to your agent a reserved Agent IDentifier (AID).

Finally, using the AID, we invoke the agent at line 13. It means that once the coroutine :meth:`AgentManager.invoke() <piaf.ptf.AgentManager.invoke()>` returns, the agent is in the ``active`` state and running.

With the :class:`~piaf.launcher.PlatformLauncher` object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using the Launcher API is a bit different since you specify the entire system before launching it. To add an agent in the simulation, you must first create a :class:`~piaf.launcher.PlatformLauncher` instance and then use the :meth:`~piaf.launcher.PlatformLauncher.add_agent()` method to add your agent by providing a :class:`~piaf.launcher.AgentDescription` object describing the agent to add: its name, the agent's class and additional arguments given to the constructor if necessary.

The following piece of code is equivalent to the one presented in the previous example:

.. code-block:: python
    :linenos:

    from piaf.agent import Agent
    from piaf.launcher import PlatformLauncher, AgentDescription


    # Create the platform launcher
    launcher = PlatformLauncher("localhost")

    # Now add an agent
    launcher.add_agent(AgentDescription("MyAgent", Agent))

    # Run the system
    launcher.run()

As you can see, the :class:`~piaf.launcher.PlatformLauncher` class takes care of most of the work, including creating and invoking your agent from the provided :class:`~piaf.launcher.AgentDescription`.

Within another agent
^^^^^^^^^^^^^^^^^^^^^

The last way to add an agent is to ask the AMS. This method allows any agent in a running platform to create and invoke another agent. For example, this is how you can start agents using the :doc:`WebAPI <webapi>`.

Asking the AMS to create and invoke an agent is done through messaging, using the standard `FIPA Request Interaction Protocol`_ with the predefined message content.

Here is a piece of code demonstrating how to handle the creation and the invocation of an agent within another agent:

.. code-block:: python
    :linenos:

    from uuid import uuid4

    from piaf.comm import ACLMessage, Performative, AID, MT_CONVERSATION_ID
    from piaf.service import AMSService, AgentCreationDescription
    from piaf.behavior import Behavior
    from piaf.agent import Agent


    class CreateAgentBehavior(Behavior):

        async def action(self) -> None:
            # Send agent creation and invocation request
            req: ACLMessage = (
                ACLMessage.Builder()
                .performative(Performative.REQUEST)
                .conversation_id(str(uuid4()))
                .receiver(AID(f"ams@{self.agent.aid.hap_name}"))
                .content(
                    [
                        AMSService.CREATE_AGENT_FUNC,
                        AgentCreationDescription(
                            Agent,
                            "MyAgent,
                            [],
                            False,
                        ),
                    ]
                )
                .build()
            )
            self.agent.send(req)

            # Wait treatment response: agree or refuse
            # Messages are filtered using the conversation ID
            agree_or_refuse = await self.agent.receive(MT_CONVERSATION_ID(req.conversation_id))
            if agree_or_refuse.acl_message.performative == Performative.REFUSE:
                raise Exception(agree_or_refuse.acl_message.content)

            # Request was accepted by the AMS, waiting for the result of the operation
            result = await self.agent.receive(MT_CONVERSATION_ID(req.conversation_id))
            if result.acl_message.performative == Performative.FAILURE:
                raise Exception(result.acl_message.content)

            # The response content contains a list of two elements:
            # 1. the request's content
            # 2. the AID of the created agent
            print(result.acl_message.content[1])

As you might have noticed, it is far more verbose than the two previous methods. The main reason is that you have to handle the messaging part!

The shown piece of code is part of behavior, which are the computation part of agents (see :doc:`behaviors`).

The important part here is the build of the request message:

1. In line 14 we set the message's performative to ``REQUEST`` to conform with the `FIPA Request Interaction Protocol`_ specification.
2. In line 17 we set the receiver to the platform's AMS.
3. In lines 18 to 28 we create the message's content. It is expected to be a list with two items: the first one is the name of the function to call on the AMS, which is ``AMSService.CREATE_AGENT_FUNC`` in our case. The second one is a description of the agent to create, supplied as a :class:`~piaf.service.AgentCreationDescription` object.


Write your agent
----------------

Knowing how to create and invoke agents, it is time to learn how to write a custom agent.

Each agent extends the :class:`~piaf.agent.Agent` class. It ensures you get the basic building blocks so your agent can evolve in a piaf platform. These building blocks are:

1. An Agent IDentifier (AID).
2. The ability to add and execute behaviors.
3. The agent's lifecycle.
4. Communication primitives.
5. Limited access to the platform's interface, mostly for lifecycle managing and extension access.

The piaf platform has been designed to keep the `logic` and the `state` of agents separated. Although you can theoretically develop your agents the way you want, we recommend you:

1. Store the agent's state as attributes.
2. Add methods to manipulate the state.
3. Create `behaviors` to implement the `logic` of your agent.

.. note:: You can find many examples of agents in the `examples` folder of the piaf project!

Here is an example of an agent that can convert a price given in dollars to euros. The agent expects a message with a performative set to ``CONVERT`` and the content to a price in dollars. Upon reception, the agent checks its conversion rate and sends back the converted price.

.. code-block:: python
    :linenos:
    :emphasize-lines: 6, 11, 12, 14, 18, 34

    from piaf.agent import Agent
    from piaf.behavior import Behavior
    from piaf.comm import MT_PERFORMATIVE, ACLMessage


    class ConvertorAgent(Agent):

        def __init__(self, aid, platform):
            super().__init__(aid, platform)

            self._dollars_to_euros = 0.93
            self.add_behavior(ConvertBehavior(self))
        
        def convert(self, price):
            return price * self._dollars_to_euros
    

    class ConvertBehavior(Behavior):

        async def action(self):
            msg = await self.agent.receive(MT_PERFORMATIVE("convert"))
            euro_price = self.agent.convert(msg.acl_message.content)

            reply = (
                ACLMessage.Builder()
                .performative("convert-reply")
                .receiver(msg.acl_message.sender)
                .conversation_id(msg.acl_message.conversation_id)
                .content(euro_price)
                .build()            
            )
            self.agent.send(reply)
        
        def done(self):
            return False

**Explanations**:

- Line 6 we extend the base class :class:`~piaf.agent.Agent`;
- Line 11 we store the exchange rate in an attribute;
- Line 12 we add the logic to our agent, aka the ``ConvertBehavior`` defined later;
- Line 14 we create a new method to quickly convert from a dollar price;
- Line 18 we define a new behavior that contains the logic of our agent. Here it means waiting for converting requests and sending back converted prices;
- Line 34 we make sure the behavior will run forever. For more details about behaviors, see :doc:`behaviors`.

.. Links definition
.. _FIPA Agent Management Specification: http://fipa.org/specs/fipa00023/SC00023K.html
.. _FIPA Request Interaction Protocol: http://fipa.org/specs/fipa00026/SC00026H.html