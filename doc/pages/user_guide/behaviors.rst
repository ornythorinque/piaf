Behaviors
=========

Behaviors are a key part of the agents and services design. They represent the active part (as opposed to the memory, which holds the agent's state) and often modify the internal state in reaction to events, being internals (computation using the state for example) or externals (a message incoming for example).

Here are two facts about behaviors in piaf:

1. An agent can have several behaviors running concurrently;
2. If an agent has no more behaviors, then it dies;

We can split behavior into two categories: one-shot and long-running. The first ones are designed to run one time and quickly. This kind of behavior is often scheduled by the second category. A good example of a long-running behavior is a behavior waiting for a particular type of message and, depending on the content, scheduling a one-shot one to handle it.

.. note:: Piaf lets you create any kind of behavior, including one-shot, infinite, and cyclic... We also provide complex behaviors like the :class:`FSMBehavior`, which uses a state machine to decide what is the next behavior to schedule. Your imagination is the limit!

Behaviors in piaf
-----------------

Each behavior in piaf must derive from the :class:`~piaf.behavior.Behavior` class. This base class exposes a few methods:

* :meth:`~piaf.behavior.Behavior.run()`
* :meth:`~piaf.behavior.Behavior.action()`
* :meth:`~piaf.behavior.Behavior.done()`
* :meth:`~piaf.behavior.Behavior.result()`

Each method has a default implementation, meaning that the base class is a valid behavior (although it does nothing).

When writing a custom behavior, it is good to have in mind the execution algorithm of behaviors, which is the following:

1. The behavior is started by the agent: the :meth:`~piaf.behavior.Behavior.run()` method is executed as it is the entry point of any behavior.
2. Then, the :meth:`~piaf.behavior.Behavior.action()` method is executed.
3. Then, the behavior checks if it should run one more time by calling the :meth:`~piaf.behavior.Behavior.done()` method. If the method returns ``False``, then go to 6.
4. The behavior gives back control to the asynchronous loop.
5. Go back to 2.
6. The :meth:`~piaf.behavior.Behavior.run()` method returns and the behavior is destroyed.

Since the default implementation of the :meth:`~piaf.behavior.Behavior.done()` method returns ``True``, the only thing to implement when writing a one-shot behavior is the :meth:`~piaf.behavior.Behavior.action()` method.

.. warning:: Unless you know what you are doing, you shouldn't override the :meth:`~piaf.behavior.Behavior.run()` as it might break your behavior.

But what about step 4 "The behavior gives back control to the asynchronous loop"? Behaviors, like many parts of the piaf library, are asynchronous tasks. Since there is only one asynchronous queue in the application, it means that every single task is queued in the same queue and executed one at a time.

If we weren't giving back control at step 4, then the currently executed behavior would keep running without letting the chance for other tasks to run.

.. note:: Generally speaking, your :meth:`~piaf.behavior.Behavior.action()` implementation should be as small as possible to minimize the time spent executing it. Thankfully, you can give back control inside the method either by waiting for the completion of another asynchronous task (like waiting for a message) or manually using :func:`asyncio.sleep()`.

Work with existing behaviors
----------------------------

The framework defines a few ready-to-use behaviors, which you can also extend to create your own:

- :class:`~piaf.behavior.Behavior`, the base for all behaviors;
- :class:`~piaf.behavior.CyclicBehavior`, which is the base class for behaviors that runs periodically in time;
- :class:`~piaf.behavior.FSMBehavior`, to create agents using a Final State Machine to decide what to do next;
- :class:`~piaf.behavior.SuicideBehavior`, for an agent to kill itself.

Behavior
^^^^^^^^

The simplest way to create a new behavior is to extend the :class:`~piaf.behavior.Behavior` class and override the :meth:`~piaf.behavior.Behavior.action()` method. By default, behaviors extending the base class are one-shot.

CyclicBehavior
^^^^^^^^^^^^^^

If you are looking for a behavior that is executed periodically, the :class:`~piaf.behavior.CyclicBehavior` is what you need. Its constructor takes an extra parameter called ``delay``. Each time the :meth:`~piaf.behavior.CyclicBehavior.done()` method is evaluated to ``True``, the behavior will sleep during ``delay`` seconds before being to run again.

.. warning:: There are no guarantees that each execution will be separated by exactly ``delay`` seconds. The final delta time between two execution will also depend on the workload of the platform.


FSMBehavior
^^^^^^^^^^^

The :class:`~piaf.behavior.FSMBehavior` is the most complex behavior available: it allows any agent to run a final state machine where each state is a behavior.

To make this happen, the :class:`~piaf.behavior.FSMBehavior` defines several methods to build the state machine:

- :meth:`~piaf.behavior.FSMBehavior.add_state()`, which takes a state name, a behavior class and, arguments given to the behavior's constructor.
- :meth:`~piaf.behavior.FSMBehavior.add_transition()`, which takes a source and a target state name and a transition function. This transition function takes one parameter, being the returned value of the previous state's behavior :meth:`~piaf.behavior.Behavior.result()` method and, must return a boolean value to decide if the current state (source) can transition to the target state. 
- :meth:`~piaf.behavior.FSMBehavior.set_initial_state()`, which sets the initial state of the state machine.

.. note:: Each time a state is reached, the associated behavior is instantiated using the supplied arguments. Those behaviors should be one-shot behaviors, with looping being implemented at the transition level.

.. warning:: The :class:`~piaf.behavior.FSMBehavior` is intended to be the only behavior running in the agent.

SuicideBehavior
^^^^^^^^^^^^^^^

This behavior will ... stop gracefully the agent. We added it because this is an often-used behavior and rather than having to write one version per project, it makes more sense to use this one!