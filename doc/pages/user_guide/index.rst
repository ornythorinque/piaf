User guide
==========
This guide will help you to understand the core functionalities of **piaf** including agents, behaviors, services, extensions, events, and much more!

.. toctree::
   :maxdepth: 2
   :caption: Concepts

   agents
   behaviors
   services
   platform
   auditing
   webapi
