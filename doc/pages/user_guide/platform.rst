Platform
========

What we call a platform is a collection of components working together to support agent execution.

There are two levels: the high-level API, which allows you to quickly define your simulation and run it, and, the low-level API which enables more customization and configuration at the cost of verbosity and complexity.

PlatformLauncher
----------------

The :class:`~piaf.launcher.PlatformLauncher` is part of the high-level API. When working with it, there are three steps to run the platform:

1. Create a :class:`~piaf.launcher.PlatformLauncher` instance.
2. Register agents using the :meth:`~piaf.launcher.PlatformLauncher.add_agent()` method and by supplying a description for your agents through a :class:`~piaf.launcher.AgentDescription` object.
3. Launch your platform with :meth:`~piaf.launcher.PlatformLauncher.run()`.

.. note:: This is the minimal setup, but the :class:`~piaf.launcher.PlatformLauncher` allows you to register services, MTPs (see :doc:`../advanced_guide/mtp`) and extensions!

Here is the piece of code:

.. code-block:: python

    from piaf.launcher import PlatformLauncher, AgentDescription
    from your.agent import CustomAgent

    # Create the launcher
    launcher = PlatformLauncher("localhost")

    # Add an agent
    launcher.add_agent(
        AgentDescription("MyAgent", CustomAgent)
    )

    # The program will run until you hit Ctrl+C
    launcher.run()


AgentPlatform
-------------

If you seek more customization (running your platform in a custom event loop, use a custom :class:`~piaf.audit.EventManager`` instance, ...), then the low-level API is what you need.

Minimal steps
^^^^^^^^^^^^^

Manually launching a platform requires following these steps:

1. Creating the platform
2. Launching it
3. Adding services and agents
    
    1. Ask the :class:`AgentManager` to create and assign an :class:`AID`
    2. Invoke the agent or service using the AID

4. Run the event loop forever
5. Tear down the platform

As you can see, there are a lot of steps!

Here is a code snippet you can use to manually launch your platform:

.. code-block:: python
    :linenos:

    import asyncio
    import logging
    from piaf.ptf import AgentPlatform

    # Create agent platform
    ap = AgentPlatform("localhost")

    async def main():
        """Coroutine that starts the platform and adds services, agents and, MTPs."""
        # Before anything, we need to start the platform. This ensures that the
        # AMS agent is created
        await ap.start()


        # Add some services
        aid = await ap.agent_manager.create(SomeServiceClass, "some_service_name", service=True)
        await ap.agent_manager.invoke(aid)

        # Add some agents
        aid = await ap.agent_manager.create(SomeAgentClass, "some_agent_name")
        await ap.agent_manager.invoke(aid)

    # Configure the logger to see things
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(logging.StreamHandler())

    # We are using asyncio library to run our example
    # The program will run until you hit Ctrl+C
    loop = asyncio.get_event_loop()
    try:
        loop.create_task(main())
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(ap.stop())
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()


AgentManager
^^^^^^^^^^^^

As you may have guessed, the :class:`~piaf.ptf.AgentManager` is the platform's component in charge of managing agents. Except when creating agents manually using the raw :class:`~piaf.ptf.AgentPlatform` object, the :class:`~piaf.ptf.AgentManager` is mostly hidden.

The purpose of this component is to:

1. Manage each agent's lifecycle
2. Create and invoke new agents
3. Keep track of each agent's run task

.. note:: The :class:`piaf.service.AMS` service mostly relies on :class:`~piaf.ptf.AgentManager` and delegates almost all of its functionalities.

Extensions
----------

Extensions are a convenient way to add additional functionalities to the basic :class:`~piaf.ptf.AgentPlatform`.

An example would be a shared connections pool to a Redis database or access to hardware stuff on the machine where the platform is deployed.

Extensions must be added to the platform before it is started (i.e. state is ``INITIALIZED``) and you are responsible for instantiating your :class:`~piaf.ptf.Extension` object. Once added, the extension is available to all agents (or only services depending on your choice) through the :attr:`~piaf.ptf.AgentPlatform.extensions()` property.

The :class:`~piaf.ptf.Extension` interface contains the bare minimum method to integrate your extension within the platform's lifecycle. Apart from that, you are free to design your extension as you want.

.. warning:: Remember when designing your extension that a unique instance of it will be shared across the platform and you should take into account concurrent accesses.