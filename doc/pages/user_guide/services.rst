Services
========

Services are a key part of the fipa specification. They are provided by the agents evolving in the platform, sometimes having unrestricted access to it.

AMS and DF
----------

The agent platform is the core, running agents and enabling communication between them. In addition to this, each platform can offer additional functionalities: hardware-related things (like accessing a thermal sensor) or software ones (often different message transfer protocols).

The fipa specification defines two services:

- the `AMS`, which is basically white pages.
- the `DF`, which is yellow pages.

The first one is mandatory and tightly coupled with the platform. The `AMS` (for agent management system) is a place where all known AIDs are stored and accessible.

.. note:: By default, any agent invoked by the platform is registered to the local AMS.

The second one, called Directory Facilitator (DF), is optional but provides useful functionalities. Every agent that wishes to advertise its services to other agents should register itself to a DF.

.. warning:: There are no guarantees on the validity and the accuracy of the information stored in the DF.

AMS
^^^

The current implementation of the Agent Management Service only supports two official functions: the **search** and the **modify** ones.

The **search** function allows any agent to search across registered agents and takes two parameters (the first one is required and the other one is optional):

- an :class:`piaf.service.AMSAgentDescription` to narrow down results
- a :class:`piaf.service.SearchConstraints` object to limit the number of results

The **modify** function allow an agent to update its record and expects one argument: an :class:`piaf.service.AMSAgentDescription` which will replace the registered record.

.. warning:: Currently, only the agent's state can be modified and a limited range of them are accepted: **ACTIVE**, **SUSPENDED**, and **UNKNOWN** (which kills gracefully the agent). Also, there are no verifications about the request sender, meaning that any agent can manipulate another agent's state.

The two other functions **register** and **deregister** are not available yet.

In addition, the AMS permits agents to create and invoke new agents thanks to the **create_agent** extension function (although it is mentioned that AMS should be able to do it). This function takes
only one parameter called `agent-creation-description` of type :class:`piaf.service.AgentCreationDescription`. The parameter describes the necessary information to create and launch an agent:

- the agent's short name
- the agent's class (which must be the fully qualified name, i.e. including the module hierarchy)
- a list of arguments to pass to the agent's constructor
- a flag to tell if the agent is a service or not (meaning it has full access to the platform API or not)

Examples
""""""""

Search::

    req: ACLMessage = (
        ACLMessage.Builder()
        .performative(Performative.REQUEST)
        .receiver(AMS_ADDR)
        .conversation_id("search_constrained")
        .content(
            [
                piaf.service.AMSService.SEARCH_FUNC,
                piaf.service.AMSAgentDescription(None, None, AgentState.ACTIVE),
                piaf.service.SearchConstraints(max_results=5),
            ]
        )
        .build()
    )

Create-agent::

    req: ACLMessage = (
        ACLMessage.Builder()
        .performative(Performative.REQUEST)
        .receiver(AMS_ADDR)
        .conversation_id("create-df")
        .content(
            [
                piaf.service.AMSService.CREATE_AGENT_FUNC,
                piaf.service.AgentCreationDescription(
                    "piaf.service.DFService", "df", is_service=True
                ),
            ]
        )
        .build()
    )


DF
^^

The current implementation of the Directory Facilitator supports several functions:

- **register**: store a new :class:`piaf.service.DFAgentDescription`
- **deregister**: deregister an agent description
- **modify**: update a stored :class:`piaf.service.DFAgentDescription`
- **search**: search across stored entries to find a service matching the provided description

More information about the functions and their arguments can be found in `the FIPA specification <http://fipa.org/specs/fipa00023/SC00023K.html>`_.

.. warning:: Federated DF and lease time are not supported yet

Example
"""""""

Registration::

    acl_message = (
        ACLMessage.Builder()
        .performative(Performative.REQUEST)
        .conversation_id("register")
        .content(
            [
                piaf.service.DFService.REGISTER_FUNC,
                piaf.service.DFAgentDescription(
                    name=YOUR_AGENT_AID,
                    services=(
                        ServiceDescription(
                            name="calendar",
                            type="calendar",
                            protocols=("fipa-request",)
                    ,),
                    protocols=("fipa-request",)
                ),
            ]
        )
        .receiver(DF_ADDR)
        .build()
    )

Search::

    request = (
        ACLMessage.Builder()
        .performative(Performative.REQUEST)
        .conversation_id("one_result")
        .receiver(DF_ADDR)
        .content(
            [
                piaf.service.DFService.SEARCH_FUNC,
                piaf.service.DFAgentDescription(
                    services=(
                        ServiceDescription(
                            name="calendar",
                            type="calendar",
                            protocols=("fipa-request",)
                    ,),
                ),
            ]
        )
        .build()
    )

Deregister::

    acl_message = (
        ACLMessage.Builder()
        .performative(Performative.REQUEST)
        .conversation_id("deregister")
        .content(
            [
                piaf.service.DFService.DEREGISTER_FUNC,
                piaf.service.DFAgentDescription(YOUR_AGENT_AID),
            ]
        )
        .receiver(DF_ADDR)
        .build()
    )


Custom service
--------------

Since services are agents there are not many things to do to create a service:

1. Create an agent
2. Define how other agents can interact with it (request/query protocols or even a custom one)
3. Register your agent to the DF and use the :class:`piaf.service.DFAgentDescription` to give information about your service

Take a look at the provided examples!
