Web API
=======

The Web API is a REST interface to interact with piaf platforms. Using a set of well-defined endpoints, you can launch platforms and interact with each instance using Web technologies.

The Web API also exposes a websocket endpoint allowing subscription to events emitted by platform instances.

.. note:: To work, the Web API used an external component: a Redis database. If you are interested in this feature, you must have a running Redis instance.

Start
-----

The piaf library provides a default REST interface that can be started easily but before doing it, you must create a `.env` file with the following parameters:

.. code-block:: bash

    REDIS_HOST = "localhost"
    REDIS_USER = "redis"
    REDIS_PASSWORD = "password"

.. note:: Replace each value to match your Redis configuration.

Now, you can start the REST interface by typing :code:`uvicorn piaf.api:app`. By default, the interface exposes documentation for each endpoint which can be found at `<http://localhost:8000/docs>`_.

Available operations
--------------------

Platforms
^^^^^^^^^

Let's start with platforms! There are three available operations for platforms:

1. Platform creating
2. List running platforms
3. Platform deletion

The first thing we can try is to start a new platform named :code:`"my-awesome-platform"`. If you take a look at the endpoint documentation, you can see that we must supply the platform's name. With curl:

.. code-block:: bash

    curl -X POST http://localhost:8000/v1/platforms \
        -H 'Content-Type: application/json' \
        -d '{"name":"my-awesome-platform"}'

Now, we can use the second available operation to list all running platforms (we should see our newly created platform!):

.. code-block:: bash

    curl -X GET http://localhost:8000/v1/platforms

You should get something like :code:`[{"name":"my-awesome-platform"}]`.

Finally, to remove a platform, we can use the last operation (platform deletion). The documentation says we must supply the platform's name as part of the URL and use the DELETE method:

.. code-block:: bash

    curl -X DELETE http://localhost:8000/v1/platforms/my-awesome-platform

If you try to list again the running platforms, you should get an empty list!

Agents
^^^^^^

Now that you know how to manage platforms, let's see what we can do with agents. At the time of writing, it exists six endpoints related to agents:

+----------------------------------------------+------+--------------------------------------------------------+
|Path                                          |Method|Description                                             |
+==============================================+======+========================================================+
|/v1/platforms/{ptf_name}/agents               |POST  |Create a new agent in the given platform.               |
+----------------------------------------------+------+--------------------------------------------------------+
|/v1/platforms/{ptf_name}/agents               |GET   |List all agents in the given platform.                  |
+----------------------------------------------+------+--------------------------------------------------------+
|/v1/platforms/{ptf_name}/agents/{aid}         |DELETE|Delete the given agent in the given platform.           |
+----------------------------------------------+------+--------------------------------------------------------+
|/v1/platforms/{ptf_name}/agents/{aid}         |GET   |Get the agent's memory in the given platform.           |
+----------------------------------------------+------+--------------------------------------------------------+
|/v1/platforms/{ptf_name}/agents/{aid}/messages|POST  |Send a message to the given agent in the given platform.|
+----------------------------------------------+------+--------------------------------------------------------+
|/v1/platforms/{ptf_name}/agents/{aid}/state   |PUT   |Update the given agent's state in the given platform.   |
+----------------------------------------------+------+--------------------------------------------------------+

.. note:: We will cover the first four endpoints. To discover how to use the two remainings, check the API documentation available at `http://localhost:8000/docs`_.

Since a platform always has a running AMS, we can start with the list operation. If you call the endpoint :code:`/v1/platforms/my-awesome-platform/agents`, you should get both the AMS and a service called API, which allows both the platform and the webserver to communicate:

.. code-block:: python

    curl -X GET http://localhost:8000/v1/platforms/my-awesome-platform/agents

.. code-block:: json

    [
        {
            "aid": {
            "name": "ams@my-awesome-platform",
            "shortname": "ams",
            "addresses": [],
            "resolvers": []
            },
            "state": "ACTIVE",
            "owner": null
        },
        {
            "aid": {
            "name": "api@my-awesome-platform",
            "shortname": "api",
            "addresses": [],
            "resolvers": []
            },
            "state": "ACTIVE",
            "owner": null
        }
    ]

Now, we can ask the AMS to create a new agent in the platform. To do so, we must describe the agent to create: its name, class, if it is a service or not, and optionally more parameters if required by the agent class's constructor.

.. note:: The AMS is capable of dynamically loading a Python module as long as it is present in the :code:`PYTHON_PATH`.

In this example, we are going to as the AMS to create the DF service, which is an optional service designed to act as yellow pages. Since it is already implemented in the piaf library, you can reproduce this example with any piaf platform:

.. code-block:: bash

    curl -X POST http://localhost:8000/v1/platforms/my-awesome-platform/agents \
        -H 'Content-Type: application/json' \
        -d '{"class_name":"piaf.service.DFService", "agent_name": "df"}'

.. code-block:: json

    {
        "name": "df@my-awesome-platform",
        "shortname": "df",
        "addresses": [],
        "resolvers": []
    }

What if we want to pause the DF service? Well, there is an endpoint for that! It allows you to update an agent's state.

.. warning:: Only a subset of states is allowed: ACTIVE and SUSPENDED.

.. code-block:: bash

    curl -X 'PUT' 'http://localhost:8000/v1/platforms/my-awesome-platform/agents/df/state' \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -d '{"state": "SUSPENDED"}'

Now, if you ask again for the list of agents in the platform, you should see that our DF agent is suspended 😎.

.. code-block:: json
    
    [
        {
            "aid": {
                "name": "df@my-awesome-platform",
                "shortname": "df",
                "addresses": [],
                "resolvers": []
            },
            "state": "SUSPENDED",
            "owner": null
        }
    ]

Last but not least, you can also kill an agent. The endpoint expects the platform's name and the agent's short name:

.. code-block:: bash

    curl -X 'DELETE' 'http://localhost:8000/v1/platforms/my-awesome-platform/agents/df'

Subscribing to events
---------------------

As you may know, piaf platforms have an event system designed to closely monitor everything in the platform. Using WebAPI, you can subscribe and unsubscribe to/from those events! To do so, the API exposes a bi-directional websocket endpoint (one per platform). The client sends subscription requests and receives the matching events until the websocket is closed or the client unsubscribes. You can subscribe to several topics, like inside the piaf platform.

.. warning:: Contrary to how events are dispatched inside the piaf platform, events are not dispatched to the topic's parents. It means that listening on :code:`.platform`` won't catch events emitted on :code:`.platform.agents`` for example.

The websocket endpoint is :code:`ws://localhost:8000/platforms/{ptf_name}/ws`` and it is a double-sided websocket carrying JSON data. The client can send the following:

.. code-block:: json

    {
        "method": "[un]subscribe",
        "topic": ".some.topic"
    }

Once an event is emitted on the subscribed topic, the client receives through the websocket a JSON representation of the EventRecord:

.. code-block:: json

    {
        "event": {
            "source": "some_source",
            "type": "some_type",
            "data": "data"
        },
        "timestamp": 1663923272,
        "topics": [".platform.agents", ".platform.agents.ams"]
    }
