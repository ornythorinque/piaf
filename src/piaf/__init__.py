"""
The root module of the `piaf` library.

It only contains the library's version.
"""
import logging

__version__ = "0.3.0-beta.1"
logging.getLogger("piaf").addHandler(logging.NullHandler())
