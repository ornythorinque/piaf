"""Configuration module."""
from __future__ import annotations

import os

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """Application settings."""

    # CORS configuration
    cors_origins: tuple[str] = ("*",)
    cors_methods: tuple[str] = ("*",)
    cors_headers: tuple[str] = ("*",)
    cors_credentials: bool = True

    model_config = SettingsConfigDict(
        env_prefix="PIAF_API_", env_file=os.getenv("PIAF_API_ENV_FILE", ".env")
    )
