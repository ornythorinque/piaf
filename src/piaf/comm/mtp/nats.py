"""
The module enbales communication between two platforms using the NATS communication
scheme.
"""
from __future__ import annotations

import asyncio
import logging
import pickle
from typing import TYPE_CHECKING, cast

from nats.errors import Error
from yarl import URL

from piaf.comm.mts import MessageContext, MessageTransportProtocol
from piaf.extensions.nats import NATSExtension

if TYPE_CHECKING:
    from nats.aio.msg import Msg
    from nats.aio.subscription import Subscription

    from piaf.comm import Message


class NATSMessageTransportProtocol(MessageTransportProtocol):
    """
    Route messages using the NATS protocol.

    Relies on the :class:`NATSExtension`.
    """

    def __init__(self, nats_ext_name: str) -> None:
        """
        Initialize a new instance of :class:`NATSMessageTransportProtocol`.

        :param nats_ext_name: the name of the NATS extension to use
        """
        super().__init__()
        self._nats_ext_name = nats_ext_name
        self._listening_condition = asyncio.Condition()
        self._subscription: None | Subscription = None
        self._stopped: bool = False
        self.logger = logging.getLogger(type(self).__name__)

    @property
    def scheme(self) -> str:
        """
        Get the scheme of the protocol.

        :return: the scheme
        """
        return "nats"

    @property
    def address(self) -> str:
        """
        Get the ACC address using the current protocol.

        :return: URL of the platform's ACC using the current protocol
        """
        return f"{self.scheme}://{self.acc.ptf_name}/acc"

    @property
    def type(self) -> str:
        """
        Get the type of this protocol (what to put in `protocol` fields).

        :return: the protocol type
        """
        return "piaf.mts.mtp.nats.aio"

    async def send(self, message: Message, address: str) -> bool:
        """
        Send the provided message to the provided address (expected to be a nats one).

        :param message: the message to send
        :param address: the nats address of the other platform's ACC
        :return: `True` if the message is sent, `False` if the provided address is not
            an NATS one or the MTP is unable to deliver the message to the remote
            platform.
        """
        parsed_url = URL(address)
        if parsed_url.scheme != self.scheme:
            return False

        rt_key = parsed_url.host
        if rt_key is None:
            return False

        # Make sure we are ready to receive responses
        async with self._listening_condition:
            while not self._stopped and self._subscription is None:
                await self._listening_condition.wait()
            if self._stopped:
                raise ConnectionError("MTP closed")

        try:
            await self._ext.publish(f"acc.{rt_key}", pickle.dumps(message, protocol=4))
        except Error as e:
            self.logger.warning("[NATS] Unable to route message.", exc_info=e)
            return False

        self.logger.debug("[NATS] Sent %s", message)
        return True

    async def start(self) -> None:
        """
        Start this MTP.

        It wil establish the connection to the NATS server and create a long running
        task to listen to incoming messages.
        """
        self._ext = cast(NATSExtension, self.acc._ptf.extensions[self._nats_ext_name])
        async with self._listening_condition:
            self._subscription = await self._ext.subscribe(
                f"acc.{self.acc.ptf_name}", self._on_message
            )

    async def stop(self) -> None:
        """
        Stop this MTP.

        Cancel the listening task and close the channel with the AMQP server.
        """
        async with self._listening_condition:
            if self._subscription is not None:
                await self._subscription.unsubscribe()
                self._subscription = None
            self._stopped = True
            self._listening_condition.notify_all()

    async def _on_message(self, msg: Msg) -> None:
        """
        Forward the incoming message to the local platform's ACC.

        .. warning:: Since this uses pickle to load the incoming data, make sure all
            peers are trusted.

        :param msg: the received message
        """
        body = pickle.loads(msg.data)  # noqa: S301
        self.logger.debug("[NATS] Received %s", body)
        self.acc.forward(body, MessageContext(msg.reply, self))
