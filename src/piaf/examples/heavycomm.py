"""
This example shows heavy exchanges between agents.

One sender with a bunch of receivers. Time elapsed between sending and receiving is
displayed.
"""  # noqa: D404
from __future__ import annotations

import time
from typing import Sequence

import piaf.agent
import piaf.behavior
import piaf.comm


class HeavySenderBehavior(piaf.behavior.Behavior):
    """
    Behavior for the sender agent.

    It builds a message containing the current time and send it to other agents.
    """

    def __init__(self, agent: HeavySenderAgent):
        """
        Initialize a new :class:`HeavySenderBehavior` object.

        :param agent: an :class:`HeavySEnderAgent` instance.
        """
        super().__init__(agent)
        self.last_call = None

    async def action(self):
        """Send an Inform message to the owner's receivers."""
        msg = (
            piaf.comm.ACLMessage.Builder()
            .performative(piaf.comm.Performative.INFORM)
            .receiver(self.agent.receivers)
            .content(time.time_ns())
            .build()
        )
        self.agent.send(msg)

    def done(self):
        """Infinite behavior."""
        return False


class ReceiveBehavior(piaf.behavior.Behavior):
    """
    Behavior for receivers.

    It unpacks the content and displays how much time elapsed between the mesage
    creation and its processing.
    """

    async def action(self):
        """
        Wait until a message is received and log how much time enlasped between the
        sending and the receiving.
        """
        msg = await self.agent.receive()
        tm = (time.time_ns() - msg.acl_message.content) / 1_000_000
        self.logger.info(
            "[%s] Msg received in %ims.",
            self.agent.aid.short_name,
            tm,
        )

    def done(self):
        """Infinite behavior."""
        return False


class HeavySenderAgent(piaf.agent.Agent):
    """
    Agent in charge of sending messages.

    You must pass a list of aids identifying the receivers.
    """

    def __init__(self, aid, platform, receivers: Sequence[piaf.comm.AID]):
        """:param receivers: the receivers"""
        super().__init__(aid, platform)
        self.receivers = receivers

        self.add_behavior(HeavySenderBehavior(self))


class SimpleReceiverAgent(piaf.agent.Agent):
    """Receiver agent."""

    def __init__(self, aid, platform):
        """
        Initialize a new :class:`SimpleReceiverAgent`.

        :param aid: this agent's identifier
        :param platform: the local platform
        """
        super().__init__(aid, platform)

        self.add_behavior(ReceiveBehavior(self))


if __name__ == "__main__":
    import logging

    import piaf.comm
    import piaf.launcher
    import piaf.ptf

    # Create the launcher
    launcher = piaf.launcher.PlatformLauncher("localhost")

    # Add receivers
    receivers: list[piaf.comm.AID] = []
    for i in range(100):
        receivers.append(piaf.comm.AID(f"rcv_{i}@localhost"))
        launcher.add_agent(
            piaf.launcher.AgentDescription(receivers[i].short_name, SimpleReceiverAgent)
        )

    # Add heavy sender
    launcher.add_agent(
        piaf.launcher.AgentDescription("sender", HeavySenderAgent, (receivers,))
    )

    # Configure logging to see things
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(logging.StreamHandler())

    # The program will run until you hit Ctrl+C
    launcher.run()
