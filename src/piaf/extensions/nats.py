"""
The module allows a platform to expose NATS primitives to agents.

See https://nats.io/
"""
from dataclasses import dataclass
from typing import TYPE_CHECKING, Awaitable, Callable

import nats
from nats.aio.msg import Msg
from nats.aio.subscription import Subscription

from piaf.ptf import Extension

if TYPE_CHECKING:
    from nats.aio.client import Client


@dataclass(frozen=True, eq=True)
class NATSExtensionSettings:
    """NATS Extension settings."""

    url: str | list[str]


class NATSExtension(Extension):
    """An extension providing NATS primitives to the platform and its agents."""

    def __init__(self, settings: NATSExtensionSettings) -> None:
        """
        Initialize a new :class:`NATSExtension` instace.

        :param settings: extension settings used to establish a connection with a NATS
            server
        """
        super().__init__()
        self._settings = settings
        self._nc: None | Client = None

    async def on_start(self) -> None:
        """Connect to the NATS instance."""
        self._nc = await nats.connect(self._settings.url)

    async def on_stop(self) -> None:
        """Close the connection to the NATS instance."""
        if self._nc is not None:
            await self._nc.close()

    async def subscribe(
        self, subject: str, cb: Callable[[Msg], Awaitable[None]] | None = None
    ) -> Subscription:
        """
        Subscribe to a NATS topic.

        :param subject: the topic's subject
        :param cb: an optional callback triggered when a message is received for the
            specified topic
        :return: a :class:`Subscription` instance
        """
        if self._nc is None:
            raise ConnectionError("No active NATS connection")
        return await self._nc.subscribe(subject, cb=cb)

    async def publish(self, subject: str, payload: bytes) -> None:
        """Publish a message on the given subject."""
        if self._nc is None:
            raise ConnectionError("No active NATS connection")
        await self._nc.publish(subject, payload)
