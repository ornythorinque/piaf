"""
Legacy module that clashes with the builtin `platform` module.

Use the :mod:`piaf.ptf` module instead.
"""
from warnings import warn

from piaf.ptf import *  # noqa: F403

warn(
    "Module 'piaf.platform' is deprecated and will be removed in piaf v1.0. Use "
    "'piaf.ptf' instead.",
    category=DeprecationWarning,
    stacklevel=2,
)
