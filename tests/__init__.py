# coding: utf-8
"""
Root module for tests.

Make the src folder available in the python path and contains shared functions.
"""
import os
import sys

# Make src folder content available in the python path
lib = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "src"))
if lib not in sys.path:
    sys.path.insert(0, lib)
