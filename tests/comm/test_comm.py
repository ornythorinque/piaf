"""Testing module for package ``piaf.comm``."""
import datetime
from typing import Sequence

import pytest
from piaf.comm import (
    AID,
    MT_AND,
    MT_CONVERSATION_ID,
    MT_ENCODING,
    MT_IN_REPLY_TO,
    MT_LANGUAGE,
    MT_NOT,
    MT_ONTOLOGY,
    MT_OR,
    MT_PERFORMATIVE,
    MT_PROTOCOL,
    MT_REPLY_TO,
    MT_REPLY_WITH,
    MT_SENDER,
    ACLMessage,
    Envelope,
    Performative,
    ReceivedObject,
)
from piaf.exceptions import IllegalArgumentError, MandatoryFieldValueError


class TestAID:
    """Class grouping all AID related tests."""

    SHORT_NAME = "tester"
    HAP_NAME = "test"
    NAME = SHORT_NAME + "@" + HAP_NAME
    ADDRESSES = ["xx://test/test"]
    RESOLVERS = ["xx://test/aid"]

    @pytest.fixture()
    def aid(self):
        """Get a full AID."""
        return AID(TestAID.NAME, TestAID.ADDRESSES, TestAID.RESOLVERS)

    def test_full_aid(self):
        """Create an AID with all necessary supplied."""
        aid = AID(TestAID.NAME, TestAID.ADDRESSES, TestAID.RESOLVERS)
        assert aid.name == TestAID.NAME
        assert aid.addresses == TestAID.ADDRESSES
        assert aid.resolvers == TestAID.RESOLVERS
        assert aid.short_name == TestAID.SHORT_NAME
        assert aid.hap_name == TestAID.HAP_NAME

    def test_aid_equality(self, aid):
        """Test equality between two AID instances."""
        assert aid == AID(TestAID.NAME)

    def test_aid_inequality(self, aid):
        """Test inequality between AIDs."""
        assert aid != AID("aid@aid")
        assert aid != AID("aid@aid", TestAID.ADDRESSES)
        assert aid != AID("aid@aid", TestAID.ADDRESSES, TestAID.RESOLVERS)

    def test_malformed_name(self):
        """You cannot instantiate an AID if name doesn't match the accepted pattern."""
        with pytest.raises(ValueError):
            AID(None)

        with pytest.raises(ValueError):
            AID("test")


class TestACLMessage:
    """Groups all tests related to ACL Messages."""

    FIXTURE_PARAMETERS = {
        "performative": Performative.AGREE,
        "sender": AID("hello@world"),
        "receiver": AID("goodbye@world"),
        "reply_to": AID("vroom@world"),
        "content": "Hello World!",
        "language": "custom",
        "encoding": "none",
        "ontology": "dummy",
        "protocol": "fipa.request",
        "conversation_id": "00110101",
        "reply_with": "Hi",
        "in_reply_to": "Hey",
        "reply_by": datetime.datetime.now(),
        "custom": "cValue",
        "X-other": "cOther",
    }

    @pytest.fixture(scope="class")
    def ACL_message(self):
        """Fixture to get an ACLMessage for comparison purpose."""
        return ACLMessage(**TestACLMessage.FIXTURE_PARAMETERS)

    @staticmethod
    def check_full_acl_message(msg: ACLMessage):
        """
        Check the provided message against all fields defines in FIXTURE_PARAMETERS.
        """
        assert msg.performative == TestACLMessage.FIXTURE_PARAMETERS["performative"]
        assert msg.sender == TestACLMessage.FIXTURE_PARAMETERS["sender"]
        assert msg.receiver == TestACLMessage.FIXTURE_PARAMETERS["receiver"]
        assert msg.reply_to == TestACLMessage.FIXTURE_PARAMETERS["reply_to"]
        assert msg.content == TestACLMessage.FIXTURE_PARAMETERS["content"]
        assert msg.language == TestACLMessage.FIXTURE_PARAMETERS["language"]
        assert msg.encoding == TestACLMessage.FIXTURE_PARAMETERS["encoding"]
        assert msg.ontology == TestACLMessage.FIXTURE_PARAMETERS["ontology"]
        assert msg.protocol == TestACLMessage.FIXTURE_PARAMETERS["protocol"]
        assert (
            msg.conversation_id == TestACLMessage.FIXTURE_PARAMETERS["conversation_id"]
        )
        assert msg.reply_with == TestACLMessage.FIXTURE_PARAMETERS["reply_with"]
        assert msg.in_reply_to == TestACLMessage.FIXTURE_PARAMETERS["in_reply_to"]
        assert msg.reply_by == TestACLMessage.FIXTURE_PARAMETERS["reply_by"]
        assert getattr(msg, "X-custom") == TestACLMessage.FIXTURE_PARAMETERS["custom"]
        assert getattr(msg, "X-other") == TestACLMessage.FIXTURE_PARAMETERS["X-other"]

    def test_acl_message_full(self, ACL_message):
        """Ensure all entries in the FIXTURE_PARAMETERS are stored in the ACLMessage."""
        TestACLMessage.check_full_acl_message(ACL_message)

    def test_acl_builder_requires_performative(self):
        """Check that an error is raised when no performative is set using the builder."""
        with pytest.raises(MandatoryFieldValueError):
            ACLMessage.Builder().content("Hello world").build()

    def test_minimal_acl_builder(self):
        """Build the minimal message possible."""
        msg = (
            ACLMessage.Builder()
            .performative(TestACLMessage.FIXTURE_PARAMETERS["performative"])
            .build()
        )

        assert msg.performative == TestACLMessage.FIXTURE_PARAMETERS["performative"]
        assert msg.sender is None
        assert msg.receiver is None
        assert msg.reply_to is None
        assert msg.content is None
        assert msg.language is None
        assert msg.encoding is None
        assert msg.ontology is None
        assert msg.protocol is None
        assert msg.conversation_id is None
        assert msg.reply_with is None
        assert msg.in_reply_to is None
        assert msg.reply_by is None

        with pytest.raises(AttributeError):
            getattr(msg, "X-custom")

        with pytest.raises(AttributeError):
            getattr(msg, "X-other")

    def test_protocol_field_requires_cvid_and_rply_by(self):
        """
        Check protocol field requirements.

        According to fipa specification http://www.fipa.org/specs/fipa00061, if
        ``protocol`` is set, ``conversation_id`` and ``reply_by`` fields should also be
        set.
        """
        with pytest.raises(MandatoryFieldValueError):
            ACLMessage.Builder().performative(Performative.PROPOSE).protocol(
                "fipa.request"
            ).build()

        with pytest.raises(MandatoryFieldValueError):
            ACLMessage.Builder().performative(Performative.PROPOSE).protocol(
                "fipa.request"
            ).conversation_id("agent01").build()

        with pytest.raises(MandatoryFieldValueError):
            ACLMessage.Builder().performative(Performative.PROPOSE).protocol(
                "fipa.request"
            ).reply_by(datetime.datetime.now()).build()

        ACLMessage.Builder().performative(Performative.PROPOSE).protocol(
            "fipa.request"
        ).conversation_id("agent01").reply_by(datetime.datetime.now()).build()

    def test_full_using_builder(self):
        """Build a full message using the builder and check all fields."""
        msg = (
            ACLMessage.Builder()
            .performative(TestACLMessage.FIXTURE_PARAMETERS["performative"])
            .sender(TestACLMessage.FIXTURE_PARAMETERS["sender"])
            .receiver(TestACLMessage.FIXTURE_PARAMETERS["receiver"])
            .reply_to(TestACLMessage.FIXTURE_PARAMETERS["reply_to"])
            .content(TestACLMessage.FIXTURE_PARAMETERS["content"])
            .language(TestACLMessage.FIXTURE_PARAMETERS["language"])
            .encoding(TestACLMessage.FIXTURE_PARAMETERS["encoding"])
            .ontology(TestACLMessage.FIXTURE_PARAMETERS["ontology"])
            .protocol(TestACLMessage.FIXTURE_PARAMETERS["protocol"])
            .conversation_id(TestACLMessage.FIXTURE_PARAMETERS["conversation_id"])
            .reply_with(TestACLMessage.FIXTURE_PARAMETERS["reply_with"])
            .in_reply_to(TestACLMessage.FIXTURE_PARAMETERS["in_reply_to"])
            .reply_by(TestACLMessage.FIXTURE_PARAMETERS["reply_by"])
            .custom("custom", TestACLMessage.FIXTURE_PARAMETERS["custom"])
            .custom("X-other", TestACLMessage.FIXTURE_PARAMETERS["X-other"])
            .build()
        )
        TestACLMessage.check_full_acl_message(msg)


class TestMessageTemplates:
    """Test all builtin message templates."""

    @pytest.fixture(scope="class")
    def aid(self):
        """Inject an AID."""
        return AID("agt@localhost")

    def test_mt_or(self):
        """Test OR message template."""
        mt = MT_OR(
            MT_PERFORMATIVE(Performative.AGREE), MT_PERFORMATIVE(Performative.REFUSE)
        )

        msg = ACLMessage.Builder().performative(Performative.CANCEL).build()
        assert not mt.apply(msg)

        msg = ACLMessage.Builder().performative(Performative.REFUSE).build()
        assert mt.apply(msg)

    def test_mt_and(self, aid):
        """Test AND message template."""
        mt = MT_AND(MT_PERFORMATIVE(Performative.AGREE), MT_SENDER(aid))

        msg = ACLMessage.Builder().performative(Performative.AGREE).build()
        assert not mt.apply(msg)

        msg = ACLMessage.Builder().performative(Performative.AGREE).sender(aid).build()
        assert mt.apply(msg)

    def test_mt_sender(self, aid):
        """Test the message template checking the ``sender`` field."""
        mt = MT_SENDER(aid)

        msg = ACLMessage.Builder().performative(Performative.AGREE).build()
        assert not mt.apply(msg)

        msg = ACLMessage.Builder().performative(Performative.AGREE).sender(aid).build()
        assert mt.apply(msg)

    def test_mt_performative(self, aid):
        """Test the message template checking the ``performative`` field."""
        mt = MT_PERFORMATIVE(Performative.AGREE)

        msg = ACLMessage.Builder().performative(Performative.NOT_UNDERSTOOD).build()
        assert not mt.apply(msg)

        msg = ACLMessage.Builder().performative(Performative.AGREE).sender(aid).build()
        assert mt.apply(msg)

    def test_mt_conversation_id(self):
        """Test the message template checking the ``conversation_id`` field."""
        mt = MT_CONVERSATION_ID("sender01")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .conversation_id("sender02")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .conversation_id("sender01")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_encoding(self):
        """Test the message template checking the ``encoding`` field."""
        mt = MT_ENCODING("utf-8")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .encoding("ascii")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .encoding("utf-8")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_in_reply_to(self):
        """Test the message template checking the ``in_reply_to`` field."""
        mt = MT_IN_REPLY_TO("previous")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .in_reply_to("next")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .in_reply_to("previous")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_language(self):
        """Test the message template checking the ``language`` field."""
        mt = MT_LANGUAGE("FIPASL")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .language("next")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .language("FIPASL")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_ontology(self):
        """Test the message template checking the ``ontology`` field."""
        mt = MT_ONTOLOGY("web")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .ontology("ships")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .ontology("web")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_protocol(self):
        """Test the message template checking the ``protocol`` field."""
        mt = MT_PROTOCOL("fipa.request")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .protocol("fipa.query")
            .conversation_id("01")
            .reply_by(datetime.datetime.now())
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .protocol("fipa.request")
            .conversation_id("01")
            .reply_by(datetime.datetime.now())
            .build()
        )
        assert mt.apply(msg)

    def test_mt_reply_with(self):
        """Test the message template checking the ``reply_with`` field."""
        mt = MT_REPLY_WITH("red-truck")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .reply_with("blue-car")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .reply_with("red-truck")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_reply_to(self):
        """Test the message template checking the ``reply_to`` field."""
        mt = MT_REPLY_TO("red-truck")

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .reply_to("handsome-ship")
            .build()
        )
        assert not mt.apply(msg)

        msg = (
            ACLMessage.Builder()
            .performative(Performative.AGREE)
            .reply_to("red-truck")
            .build()
        )
        assert mt.apply(msg)

    def test_mt_not(self):
        """Test the message template NOT."""
        mt = MT_NOT(MT_PERFORMATIVE(Performative.AGREE))

        msg = ACLMessage.Builder().performative(Performative.AGREE).build()
        assert not mt.apply(msg)

        msg = ACLMessage.Builder().performative(Performative.ACCEPT_PROPOSAL).build()
        assert mt.apply(msg)


class TestEnvelope:
    """Gather all tests related to :class:`piaf.comm.Envelope`."""

    @pytest.fixture()
    def envelope(self):
        """Get a new :class:`Envelope` object."""
        return (
            Envelope.Builder()
            .to((AID("dummy@localhost"),))
            .from_(AID("me@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.datetime.now())
            .comments("Comments")
            .playload_length("15")
            .playload_encoding("UTF-8")
            .custom("X-custom", "value")
            .build()
        )

    def test_required_parameters(self, envelope: "Envelope"):
        """
        Try to access to all mandatory fields. They should be defined.

        :param envelope: the injected :class:`Envelope` object
        """
        # Mandatory fields that should have a value
        assert isinstance(envelope.to, Sequence)
        assert isinstance(envelope.from_, AID)
        assert isinstance(envelope.acl_representation, str)
        assert isinstance(envelope.date, datetime.datetime)

        # Existing attributes, not mandatory. Check types if defined
        if envelope.comments is not None:
            assert isinstance(envelope.comments, str)
        if envelope.playload_length is not None:
            assert isinstance(envelope.playload_length, str)
        if envelope.playload_encoding is not None:
            assert isinstance(envelope.playload_encoding, str)
        if envelope.received is not None:
            assert isinstance(envelope.received, ReceivedObject)
        if envelope.intended_receiver is not None:
            assert isinstance(envelope.intended_receiver, Sequence)

        # Existing attribute, type not defined yet
        envelope.transport_behaviour

    def test_revisions(self, envelope: "Envelope"):
        """
        Test multiple occurrences of the same field.

        We check that:
         - revision number is updated
         - field's value is updated
         - field's old value is still accessible through previous revision
        """
        expected = "Comments have been updated"
        old = envelope.comments

        envelope.add_revision({Envelope.Builder.COMMENTS: expected})

        assert envelope.comments == expected
        assert envelope.get_field_value(Envelope.Builder.COMMENTS) == expected
        assert envelope.last_revision_number == 1
        assert envelope.get_field_value(Envelope.Builder.COMMENTS, 0) == old

    def test_custom_fields(self, envelope: "Envelope"):
        """
        Test custom fields in envelope.

        We check that:
         - custom fields are saved
         - custom fields must start with FIPA prefix 'X-'
        """
        with pytest.raises(IllegalArgumentError):
            envelope.add_revision({"custom field": 10})

        envelope.add_revision({"X-Custom2": 10})
        assert envelope.get_field_value("X-Custom2") == 10


class TestReceivedObject:
    """Gather all tests related to :class:`ReceivedObject`."""

    def test_minimal_ro(self):
        """Check mandatory fields."""
        with pytest.raises(MandatoryFieldValueError):
            ReceivedObject.Builder().build()

        with pytest.raises(MandatoryFieldValueError):
            ReceivedObject.Builder().by("http://localhost/acc").build()

        with pytest.raises(MandatoryFieldValueError):
            ReceivedObject.Builder().date(datetime.datetime.now()).build()

        ReceivedObject.Builder().by("http://localhost/acc").date(
            datetime.datetime.now()
        ).build()

    def test_custom_fields(self):
        """
        Check custom fields.

        We check that:
         - custom fields are saved
         - custom fields must start with FIPA prefix 'X-'
        """
        with pytest.raises(IllegalArgumentError):
            ReceivedObject.Builder().by("http://localhost/acc").date(
                datetime.datetime.now()
            ).custom("not-working", "any").build()

        ro = (
            ReceivedObject.Builder()
            .by("http://localhost/acc")
            .date(datetime.datetime.now())
            .custom("X-working", "any")
            .build()
        )

        assert getattr(ro, "X-working") == "any"
        with pytest.raises(AttributeError):
            getattr(ro, "non-existing")


class TestReceivedObjectBuilder:
    """Class gathering all tests related to the :class:`ReceivedObject.Builder` class."""

    BY = "Me"
    DATE = datetime.datetime.now()
    FROM = "Other"
    ID = "ID"
    VIA = "http://remotehost/acc"
    CUSTOM = ("X-Custom", "CustomValue")

    @pytest.fixture()
    def rob(self) -> ReceivedObject.Builder:
        return (
            ReceivedObject.Builder()
            .by(TestReceivedObjectBuilder.BY)
            .date(TestReceivedObjectBuilder.DATE)
            .from_(TestReceivedObjectBuilder.FROM)
            .id(TestReceivedObjectBuilder.ID)
            .via(TestReceivedObjectBuilder.VIA)
            .custom(*TestReceivedObjectBuilder.CUSTOM)
        )

    def test_build_mandatory_fields(self):
        """Check if builder's build method raises an exception if mandatory fields are not set."""
        builder = ReceivedObject.Builder()

        with pytest.raises(MandatoryFieldValueError):
            builder.build()

        builder.by("Me")
        with pytest.raises(MandatoryFieldValueError):
            builder.build()

        builder.by(None).date(datetime.datetime.now())
        with pytest.raises(MandatoryFieldValueError):
            builder.build()

    def test_built_object(self, rob: ReceivedObject.Builder):
        """Ensure built object have correct values."""
        ro = rob.build()

        assert ro.by == TestReceivedObjectBuilder.BY
        assert ro.date == TestReceivedObjectBuilder.DATE
        assert ro.from_ == TestReceivedObjectBuilder.FROM
        assert ro.id == TestReceivedObjectBuilder.ID
        assert ro.via == TestReceivedObjectBuilder.VIA
        assert (
            getattr(ro, TestReceivedObjectBuilder.CUSTOM[0])
            == TestReceivedObjectBuilder.CUSTOM[1]
        )
