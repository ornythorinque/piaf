"""Testing module for :mod:`piaf.comm.mts`."""
from __future__ import annotations

import asyncio
import urllib.parse
from datetime import datetime

import piaf.exceptions as ex
import pytest
from piaf.audit import ACC_TOPIC, AGENTS_PARENT_TOPIC, Event, EventManager, Topic
from piaf.comm import AID, MT_PERFORMATIVE, ACLMessage, Envelope, Message, Performative
from piaf.comm.mts import (
    AgentCommunicationChannel,
    MailBox,
    MessageContext,
    MessageTransportProtocol,
    PlayloadParser,
)

from tests.test_audit import DummySubscriber


class _DummyPlayloadParser(PlayloadParser, cmp_name="dummy"):
    def dump(self, data, encoding="utf-8"):
        return bytearray()

    def load(self, data, encoding="utf-8"):
        return None


def test_p_parsers_gathering():
    assert PlayloadParser.get_parser("dummy") == _DummyPlayloadParser


class TestMailBox:
    """Group all tests related to the mailbox."""

    @pytest.fixture()
    def nu_msg(self) -> Message:
        """Get a new message with performative NOT_UNDERSTOOD."""
        return Message(
            envelope=None,
            acl_message=ACLMessage.Builder()
            .performative(Performative.NOT_UNDERSTOOD)
            .build(),
        )

    @pytest.fixture()
    def ap_msg(self) -> Message:
        """Get a new message with performative ACCEPT_PROPOSAL."""
        return Message(
            envelope=None,
            acl_message=ACLMessage.Builder()
            .performative(Performative.ACCEPT_PROPOSAL)
            .build(),
        )

    @pytest.fixture()
    def mailbox(self) -> MailBox:
        """Get a fresh new mailbox."""
        return MailBox()

    async def test_put_get_no_template(self, mailbox: MailBox, nu_msg: Message):
        """Put a message in the mailbox and try to retrieve it."""
        await mailbox.put(nu_msg)
        assert await mailbox.get() == nu_msg

    async def test_put_get_with_template(
        self, mailbox: MailBox, nu_msg: Message, ap_msg: Message
    ):
        """Test filtering messages when invoking get method."""
        await mailbox.put(nu_msg)
        await mailbox.put(ap_msg)

        assert (
            await mailbox.get(MT_PERFORMATIVE(Performative.ACCEPT_PROPOSAL)) == ap_msg
        )
        assert await mailbox.get() == nu_msg

    async def test_get_with_template_put(
        self, mailbox: MailBox, nu_msg: Message, ap_msg: Message
    ):
        """Check mailbox in a blocking way, then push the desired message."""

        async def _push():
            await asyncio.sleep(0.2)  # Delay push to ensure get is blocking
            await mailbox.put(nu_msg)

            await asyncio.sleep(0.2)
            await mailbox.put(ap_msg)

        asyncio.create_task(_push())
        assert (
            await mailbox.get(MT_PERFORMATIVE(Performative.ACCEPT_PROPOSAL)) == ap_msg
        )

    async def test_put_get_nowait_no_template(self, mailbox: MailBox, nu_msg: Message):
        """Use get_nowait retrieving method, without template."""
        assert await mailbox.get_nowait() is None

        await mailbox.put(nu_msg)
        assert await mailbox.get_nowait() == nu_msg
        assert await mailbox.get_nowait() is None


class TestAgentCommunicationChannel:
    """Gather all tests related to the :class:AgentCommunicationChannel`."""

    class DummyPlatform:
        """Dummy mocked platform. Name is 'localhost'."""

        def __init__(self) -> None:
            """Store an :class:`EventManager` instance."""
            self.evt_manager = EventManager()

        @property
        def name(self):
            """Get platform name."""
            return "localhost"

    class DummyMTP(MessageTransportProtocol):
        """Dummy MTP."""

        def __init__(self) -> None:
            super().__init__()
            self._cond = asyncio.Condition()
            self._msg = None

        async def check_received(self) -> bool:
            async with self._cond:
                await self._cond.wait_for(lambda: self._msg is not None)
                return True

        async def check_not_received(self, delay=1) -> bool:
            await asyncio.sleep(delay)
            return self._msg is None

        async def send(self, msg: Message, address: str) -> bool:
            async with self._cond:
                self._msg = msg
                self._cond.notify_all()
                return True

        @property
        def address(self):
            return f"{self.scheme}://localhost/acc"

        @property
        def type(self) -> str:
            return self.scheme

        async def start(self) -> None:
            pass

        async def stop(self) -> None:
            pass

    class DummyHttpMTP(DummyMTP):
        @property
        def scheme(self):
            return "http"

    class DummyFtpMTP(DummyMTP):
        @property
        def scheme(self):
            return "ftp"

    @pytest.fixture()
    def no_ir_single(self) -> Message:
        """
        :class:`Message` containing an :class:`Envelope` without intended-receiver.

        The to parameter of the envelope contains only one :class:`AID`.
        """
        return Message(
            envelope=Envelope.Builder()
            .to((AID("dummy@localhost"),))
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .build(),
            acl_message=ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver((AID("dummy@remotehost"),))
            .reply_with("foo")
            .build(),
        )

    @pytest.fixture()
    def ir_single(self) -> Message:
        """
        :class:`Message` containing an :class:`Envelope` with an intended-receiver.

        The intended_parameter parameter of the envelope contains only one :class:`AID`.
        """
        return Message(
            envelope=Envelope.Builder()
            .to((AID("dummy@localhost"),))
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .intended_receiver((AID("dummy@localhost"),))
            .build(),
            acl_message=ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver((AID("dummy@localhost"),))
            .reply_with("foo")
            .build(),
        )

    @pytest.fixture()
    def ir_mult(self) -> Message:
        """
        :class:`Message` containing an :class:`Envelope` with an intended-receiver.

        The intended_parameter parameter of the envelope contains multiple :class:`AID`s.
        """
        return Message(
            envelope=Envelope.Builder()
            .to((AID("dummy@localhost"),))
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .intended_receiver(
                (
                    AID("foo@remotehost"),
                    AID("dummy@localhost"),
                )
            )
            .build(),
            acl_message=ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver(
                (
                    AID("foo@remotehost"),
                    AID("dummy@localhost"),
                )
            )
            .reply_with("foo")
            .build(),
        )

    @pytest.fixture()
    def r_agt_one_ta(self):
        """
        :class:`Message` containing an :class:`Envelope` addressed to a single receiver.

        Only one transport address is specified.
        """
        return Message(
            envelope=Envelope.Builder()
            .to((AID("rdummy@remotehost", ("http://remotehost/acc",)),))
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .build(),
            acl_message=ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver((AID("rdummy@remotehost", ("http://remotehost/acc",)),))
            .reply_with("foo")
            .build(),
        )

    @pytest.fixture()
    def no_ir_mr(self) -> Message:
        """Get a message without intended receiver field and multiple AIDs in to field."""
        return Message(
            envelope=Envelope.Builder()
            .to((AID("one@localhost"), AID("two@localhost"), AID("three@localhost")))
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .build(),
            acl_message=ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver(
                (AID("one@localhost"), AID("two@localhost"), AID("three@localhost"))
            )
            .reply_with("foo")
            .build(),
        )

    @pytest.fixture()
    def r_agt_mult_ta(self) -> Message:
        """Get a message where to field is filled with one AID containing multiple transport addresses."""
        envelope = (
            Envelope.Builder()
            .to(
                (
                    AID(
                        "rcv@remotehost",
                        ("ftp://remotehost/acc", "http://remotehost/acc"),
                    ),
                )
            )
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .build()
        )

        message = (
            ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver(
                AID(
                    "rcv@remotehost",
                    ("ftp://remotehost/acc", "http://remotehost/acc"),
                )
            )
            .reply_with("foo")
            .build()
        )
        return Message(envelope, message)

    @pytest.fixture()
    def r_mult_agt(self) -> Message:
        """Get a message where to field is filled with multiple AIDs."""
        message = (
            ACLMessage.Builder()
            .performative(Performative.PROPOSE)
            .conversation_id("s1")
            .sender(AID("sender@localhost"))
            .receiver(
                (
                    AID(
                        "rcv@remotehost",
                        ("ftp://remotehost/acc"),
                    ),
                    AID(
                        "dummy@localhost",
                        ("ftp://localhost/acc,"),
                    ),
                )
            )
            .reply_with("foo")
            .build()
        )
        envelope = (
            Envelope.Builder()
            .to(
                (
                    AID(
                        "rcv@remotehost",
                        ("ftp://remotehost/acc",),
                    ),
                    AID(
                        "dummy@localhost",
                        ("ftp://localhost/acc,"),
                    ),
                )
            )
            .from_(AID("sender@localhost"))
            .acl_representation("fipa.acl.rep.string.std")
            .date(datetime.now())
            .build()
        )
        return Message(envelope, message)

    @pytest.fixture()
    def acc(self) -> AgentCommunicationChannel:
        """Get a fresh new :class:`AgentCommunicationChannel` object."""
        return AgentCommunicationChannel(TestAgentCommunicationChannel.DummyPlatform())

    def test_register_agent_or_service(self, acc: AgentCommunicationChannel) -> None:
        """
        Test agent or service registration.

        After registration, the ACC should have associated the agent or service name to the mailbox.
        """
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)
        assert acc._knows["dummy"] is mb

    def test_register_agent_or_service_duplicate(
        self, acc: AgentCommunicationChannel
    ) -> None:
        """
        Test agent or service registration.

        In case of a duplicated name, the registration should reject the demand.
        """
        acc.register_agent_or_service("dummy", None)
        with pytest.raises(ex.DuplicatedNameError):
            acc.register_agent_or_service("dummy", None)

    def test_forget_agent_or_service(self, acc: AgentCommunicationChannel) -> None:
        """
        Test agent or service forget.

        After forget, it should be possible to register again a service or an agent with the forget name.
        """
        acc.register_agent_or_service("dummy", None)
        acc.forget_agent_or_service("dummy")
        acc.register_agent_or_service("dummy", None)

    async def test_forward_no_intended_receiver(
        self, acc: AgentCommunicationChannel, no_ir_single: Message
    ) -> None:
        """
        Test message forwarding when intended_receiver field is empty.

        The final message envelope should contain a filled intended_receiver field.
        """
        """
        Forward a message to a local agent.

        Must generate the ``intended-receiver`` field from the ``to`` field.
        """

        # Register an agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(no_ir_single)

        assert await mb.get() is not None

    async def test_forward_no_intended_receiver_multiple_receivers(
        self, acc: AgentCommunicationChannel, no_ir_mr: Message
    ) -> None:
        """
        Forward a message to local agents.

        The intended-receiver envelope field is empty.
        """
        # Register some agents
        mb_one = MailBox()
        mb_two = MailBox()
        mb_three = MailBox()

        acc.register_agent_or_service("one", mb_one)
        acc.register_agent_or_service("two", mb_two)
        acc.register_agent_or_service("three", mb_three)

        # Forward message
        acc.forward(no_ir_mr)

        assert await mb_one.get() is not None
        assert await mb_two.get() is not None
        assert await mb_three.get() is not None

    async def test_forward_intended_receiver(
        self, acc: AgentCommunicationChannel, ir_single: Message
    ) -> None:
        """
        Forward a message to a local agent.

        The intended-receiver envelope field is set with one :class:`AID`
        """
        # Register an agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(ir_single)

        assert await mb.get() is not None

    async def test_forward_multiple_intended_receiver(
        self, acc: AgentCommunicationChannel, ir_mult: Message
    ) -> None:
        """
        Forward a message to a local agent.

        The intended-receiver envelope field is set with multiple :class:`AID`s.
        """
        # Register an agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(ir_mult)

        assert await mb.get() is not None

    async def test_forward_has_received_field(
        self, acc: AgentCommunicationChannel, ir_single: Message
    ) -> None:
        """Check that the ACC is adding an entry in the received field."""
        # Register an agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(ir_single)

        msg = await mb.get()
        assert msg.envelope.received is not None

        url = urllib.parse.urlparse(msg.envelope.received.by)
        assert url.netloc == "localhost"
        assert url.path == "/acc"

        assert msg.envelope.received is not None

        acc_id = msg.envelope.received.id
        acc.forward(ir_single)
        msg = await mb.get()
        assert acc_id != msg.envelope.received.id

    async def test_forward_has_received_field_with_context(
        self, acc: AgentCommunicationChannel, ir_single: Message
    ) -> None:
        """
        Check that the ACC is adding an entry in the received field.

        The message is forwarded with a context, thus we expect more information in the received field.
        """
        # Register an agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(
            ir_single,
            MessageContext(
                from_="http://remotehost/acc",
                via=TestAgentCommunicationChannel.DummyHttpMTP(),
            ),
        )

        msg = await mb.get()
        assert msg.envelope.received is not None

        assert msg.envelope.received.via == "http"  # MTP's type (here <=> scheme)
        assert msg.envelope.received.from_ == "http://remotehost/acc"

    async def test_remote_agent_single_transport_address(
        self, acc: AgentCommunicationChannel, r_agt_one_ta: Message
    ) -> None:
        """Test forwarding a message to a remote agent."""
        # Register dummy http MTP
        mtp = TestAgentCommunicationChannel.DummyHttpMTP()
        await acc.register_mtp(mtp)

        # Forward message
        acc.forward(r_agt_one_ta, None)

        assert await mtp.check_received()

    async def test_remote_agent_multiple_transport_address_no_error(
        self, acc: AgentCommunicationChannel, r_agt_mult_ta: Message
    ) -> None:
        """Forward to a remote agent with multiple transport addresses."""
        # Register dummy http MTP
        mtp = TestAgentCommunicationChannel.DummyHttpMTP()
        await acc.register_mtp(mtp)

        # Forward message
        acc.forward(r_agt_mult_ta, None)

        assert await mtp.check_received()

    async def test_remote_agent_multiple_transport_address_check_order(
        self, acc: AgentCommunicationChannel, r_agt_mult_ta: Message
    ) -> None:
        """Forward to a remote agent with multiple transport addresses."""
        # Register dummy MTPs
        mtp_ftp = TestAgentCommunicationChannel.DummyFtpMTP()
        mtp_http = TestAgentCommunicationChannel.DummyHttpMTP()
        await acc.register_mtp(mtp_ftp)
        await acc.register_mtp(mtp_http)

        # Forward message
        acc.forward(r_agt_mult_ta, None)

        assert await mtp_ftp.check_received()

    async def test_remote_multiple_agents(
        self, acc: AgentCommunicationChannel, r_mult_agt: Message
    ) -> None:
        """Forward to multiple agents, both remote and local."""
        # Register dummy MTPs
        mtp_ftp = TestAgentCommunicationChannel.DummyFtpMTP()
        await acc.register_mtp(mtp_ftp)

        # Register an agent and hack mailbox
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Forward message
        acc.forward(r_mult_agt, None)

        assert await mtp_ftp.check_received()
        await mb.get()

    async def test_forward_failure(
        self, acc: AgentCommunicationChannel, r_agt_mult_ta: Message
    ) -> None:
        """Forward failure (no matching MTP), one agent, multiple transport addresses."""
        # Register a mailbox
        mb = MailBox()
        acc.register_agent_or_service("sender", mb)

        # Forward message
        acc.forward(r_agt_mult_ta, None)

        # We should receive a failure message
        msg = await mb.get(MT_PERFORMATIVE(Performative.FAILURE))

        # Assert properties on the received message
        assert msg.acl_message.conversation_id == "s1"
        assert msg.acl_message.in_reply_to == "foo"

    async def test_forward_multiple_failures(
        self, acc: AgentCommunicationChannel, r_mult_agt: Message
    ) -> None:
        """Forward failure (no matching MTP, unknown local agent), multiple agents, one transport address."""
        # Register a mailbox
        mb = MailBox()
        acc.register_agent_or_service("sender", mb)

        # Forward message
        acc.forward(r_mult_agt, None)

        # We should receive a first failure message
        msg = await mb.get(MT_PERFORMATIVE(Performative.FAILURE))

        # Assert properties on the received message
        assert msg.acl_message.conversation_id == "s1"
        assert msg.acl_message.in_reply_to == "foo"

        # We should receive a second failure message
        msg = await mb.get(MT_PERFORMATIVE(Performative.FAILURE))

        # Assert properties on the received message
        assert msg.acl_message.conversation_id == "s1"
        assert msg.acl_message.in_reply_to == "foo"

    async def test_register_mtp(self, acc: AgentCommunicationChannel) -> None:
        """Ensure an event is fired when a new MTP is registered."""
        from tests.test_audit import DummySubscriber

        sub = DummySubscriber()
        acc._ptf.evt_manager.subscribe_to(sub, ACC_TOPIC)

        mtp = TestAgentCommunicationChannel.DummyFtpMTP()
        await acc.register_mtp(mtp)
        assert Event("acc", "mtp_registration", mtp.scheme) in (
            r.event for r in sub.records
        )

    async def test_reception_event_is_fired(
        self, acc: AgentCommunicationChannel, ir_single: Message
    ) -> None:
        """Ensure an event is fired when a message is put in an agent's mailbox."""
        # Register fake agent
        mb = MailBox()
        acc.register_agent_or_service("dummy", mb)

        # Listen to events
        sub = DummySubscriber()
        topic = Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "dummy.messages")
        acc._ptf.evt_manager.subscribe_to(
            sub,
            topic,
        )

        # Forward message
        acc.forward(ir_single)

        # Await message reception
        await mb.get()

        # Asserts
        assert len(sub.records) == 1
        assert sub.records[0].event == Event(
            "dummy@localhost", "message_reception", ir_single.acl_message
        )
