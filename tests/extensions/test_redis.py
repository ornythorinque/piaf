from __future__ import annotations

from unittest.mock import AsyncMock, MagicMock

import pytest

from piaf.extensions.redis import RedisConnectionPoolExtension


class MockConnectionPool:
    @staticmethod
    def from_url(uri: str, **options) -> MockConnectionPool:
        return AsyncMock()


class TestRedisConnectionPoolExtension:
    def test_client_not_started(self) -> None:
        ext = RedisConnectionPoolExtension(MagicMock())

        with pytest.raises(ConnectionError):
            _ = ext.client
