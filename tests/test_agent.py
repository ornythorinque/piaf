import asyncio

import piaf.comm
from piaf.agent import Agent, AgentState
from piaf.audit import AGENTS_PARENT_TOPIC, Event, EventManager, Topic
from piaf.ptf import AgentPlatform

from tests.test_audit import DummySubscriber
from tests.test_service import HelperAgent


class PlatformFacadeMock:
    def __init__(self):
        self.agt_state = AgentState.UNKNOWN
        self.item_sent = None
        self.get_nowait_called = False
        self.evt_manager = EventManager()

    async def suspend(self, agent: piaf.comm.AID) -> None:
        self.agt_state = AgentState.SUSPENDED

    async def wait(self, agent: piaf.comm.AID) -> None:
        self.agt_state = AgentState.WAITING

    async def quit(self, agent: piaf.comm.AID) -> None:
        self.agt_state = AgentState.UNKNOWN

    def send(self, message: piaf.comm.Message) -> None:
        self.item_sent = message.acl_message

    def get_state(self, agent: piaf.comm.AID) -> piaf.agent.AgentState:
        return self.agt_state

    @property
    def name(self):
        return "localhost"


async def test_agent():
    plt = PlatformFacadeMock()
    aid = piaf.comm.AID("test@localhost")
    agt = Agent(aid, plt)

    fut = asyncio.create_task(agt.run())
    await asyncio.sleep(0.1)
    assert fut.done()

    await agt.wait()
    assert agt.state == AgentState.WAITING

    await agt.suspend()
    assert agt.state == AgentState.SUSPENDED

    msg = (
        piaf.comm.ACLMessage.Builder()
        .performative("test")
        .receiver(piaf.comm.AID("friend@remote"))
        .build()
    )
    agt.send(msg)
    assert msg == plt.item_sent
    assert agt.aid == aid
    assert id(agt.aid) != id(aid)


async def test_msg_sending():
    """Ensure an event is fired when an agent sends a message."""
    # Create and start platform
    plt = AgentPlatform("localhost")
    await plt.start()

    # Create sender and receiver agents
    queue_in = asyncio.Queue()
    queue_out = asyncio.Queue()

    aid = await plt.agent_manager.create(
        HelperAgent, "receiver", None, queue_out, is_service=False
    )
    await plt.agent_manager.invoke(aid)

    aid = await plt.agent_manager.create(
        HelperAgent, "sender", queue_in, None, is_service=False
    )
    await plt.agent_manager.invoke(aid)

    # Add subscriber
    sub = DummySubscriber()
    plt.evt_manager.subscribe_to(
        sub,
        Topic.resolve_from_parent_topic(
            AGENTS_PARENT_TOPIC, f"{aid.short_name}.messages"
        ),
    )

    # Send a message
    msg = (
        piaf.comm.ACLMessage.Builder()
        .performative("test")
        .sender(aid)
        .receiver(piaf.comm.AID("receiver@localhost"))
        .content("Some content")
        .build()
    )
    await queue_in.put(msg)
    await queue_out.get()  # Wait until the message is received

    assert len(sub.records) == 1
    assert sub.records[0].event == Event(aid.name, "message_sending", msg)

    await plt.stop()
