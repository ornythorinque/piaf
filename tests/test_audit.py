# coding: utf-8
"""A collection of tests related to the `piaf.events` module."""
from __future__ import annotations

from typing import List

from pytest import fixture, raises

from piaf.audit import Event, EventManager, EventRecord, Subscriber, Topic


class TestTopic:
    """Gather tests related to the :class:`piaf.events.Topic` class."""

    def test_properties(self):
        """Make sure topics are well created and immutable."""
        t = Topic.from_str("topic")

        assert t.name == "topic"
        assert t.parent == Topic("", None)

        with raises(AttributeError):
            t.name = "tit"

        with raises(AttributeError):
            t.parent = Topic("tata", None)

    def test_eq(self) -> None:
        """Test equality and hash relations."""
        t1 = Topic.from_str("topic")
        t2 = Topic.from_str("topic")

        assert t1 == t2
        assert hash(t1) == hash(t2)

    def test_cache(self) -> None:
        """Ensure topics are cached."""
        t1 = Topic.from_str("topic")
        t2 = Topic.from_str("topic")

        assert id(t1) == id(t2)
        assert id(t1.parent) == id(Topic.from_str(""))

    def test_from_str(self) -> None:
        """Ensure the :meth:`Topic.from_str` method behaves correctly."""
        assert Topic.from_str("parent.child") == Topic(
            "child", Topic("parent", Topic("", None))
        )

    def test_resolve_from_parent_topic(self) -> None:
        """Ensure the :meth:`Topic.resolve_from_parent_topic` method behaves correctly."""
        parent = Topic.from_str("parent")
        assert Topic.resolve_from_parent_topic(parent, "child") == Topic.from_str(
            "parent.child"
        )

    def test_true_division(self) -> None:
        parent = Topic.from_str("parent")
        assert parent / "child" == Topic.from_str("parent.child")

        with raises(TypeError):
            parent / 2


class DummySubscriber(Subscriber):
    """A dummy subscriber that stores in a list each record."""

    def __init__(self) -> None:
        """Initialize the subscriber with an empty records list."""
        self.records: List[EventRecord] = []

    async def on_event(self, event_record: EventRecord) -> None:
        """
        Append the given record in the internal records list.

        :param event_record: the event record to append
        """
        self.records.append(event_record)


class TestEventManager:
    """Gather all tests related to the :class:`piaf.events.EventManager` class."""

    @fixture
    def em(self) -> EventManager:
        """
        Get a fresh instance of :class:`piaf.events.EventManager`.

        :return: a new instance, never `None`.
        """
        return EventManager()

    async def test_publish(self, em: EventManager) -> None:
        """Ensure the publish/notify algorithm works as expected."""

        evt = Event("agent", "message", "data")
        topic1 = Topic.from_str("topic")
        topic2 = Topic.from_str("nice.topic")

        # Create subscribers
        sub1 = DummySubscriber()
        sub2 = DummySubscriber()
        sub3 = DummySubscriber()

        # Subscribe
        em.subscribe_to(sub1, topic1)
        em.subscribe_to(sub2, [Topic.from_str(""), topic2])
        em.subscribe_to(sub3, Topic.from_str("other_topic"))

        # Event is published to the subscriber
        await em.publish(evt, (topic1, topic2))
        assert evt in (record.event for record in sub1.records)

        # Event is also published in the hierarchy of the topic + only once
        assert evt in (record.event for record in sub2.records)
        assert len(sub2.records) == 1

        # Event is not published in topics outside the hierarchy
        assert len(sub3.records) == 0

        # Records is in fact a unique record
        records = set(sub1.records + sub2.records + sub3.records)
        assert len(records) == 1
        assert sub1.records[0].topics == (topic1, topic2)
