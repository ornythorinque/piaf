# coding: utf-8
import asyncio
import unittest.mock as mock

import pytest

import piaf.agent
import piaf.behavior
import piaf.comm

# State sync condition
_sync = asyncio.Condition()

# ####### Mock piaf.agent.Agent
agent = mock.Mock()

# # Properties
type(agent).aid = mock.PropertyMock(return_value=piaf.comm.AID("agent@localhost"))
type(agent).state = mock.PropertyMock(return_value=piaf.agent.AgentState.ACTIVE)
type(agent).state_sync = mock.PropertyMock(return_value=_sync)

# # Methods
agent.add_behavior = mock.Mock(side_effect=lambda b: asyncio.create_task(b.run()))


async def quit():
    agent.has_quit = True


agent.quit = mock.Mock(side_effect=quit)
# ###### End


class DummyBehavior(piaf.behavior.Behavior):
    def __init__(self, agent, *args, **kwargs):
        super().__init__(agent, *args, **kwargs)
        self.count = 0

    async def action(self):
        self.count += 1


class DummyCyclicBehavior(piaf.behavior.CyclicBehavior):
    def __init__(self, agent, delay, *args, **kwargs):
        super().__init__(agent, delay, *args, **kwargs)
        self.count = 0

    async def action(self):
        self.count += 1

    def done(self):
        return self.count >= 10


class ABehavior(piaf.behavior.Behavior):
    def __init__(self, agent, out, counter, *args, **kwargs):
        super().__init__(agent, *args, **kwargs)
        self.out = out
        self.counter = counter
        self.val = None

    async def action(self):
        self.val = next(self.counter)
        self.out.append(self.val)

    def result(self):
        return self.val


async def test_behavior_one_shot():
    bhv = DummyBehavior(agent)
    await bhv.run()

    assert bhv.count == 1
    assert bhv.result() is None


async def test_behavior_cyclic():
    bhv = DummyCyclicBehavior(agent, 0.05)
    task = asyncio.create_task(bhv.run())
    await asyncio.sleep(0.3)

    task.cancel()
    assert bhv.count >= 5
    assert bhv.result() is None

    bhv = DummyCyclicBehavior(agent, 0.05)
    await bhv.run()

    assert bhv.count == 10
    assert bhv.result() is None


def get_fsm(out, counter):
    bhv = piaf.behavior.FSMBehavior(agent)

    bhv.add_state("A", ABehavior, args=(out, counter))
    bhv.add_state("B", ABehavior, args=(out, counter))
    bhv.add_state("C", ABehavior, args=(out, counter))
    bhv.add_state("D", ABehavior, args=(out, counter), final=True)

    bhv.set_initial_state("A")

    bhv.add_transition("A", "B", lambda r: r == "B")
    bhv.add_transition("A", "C", lambda r: r == "C")
    bhv.add_transition("B", "A", lambda r: r == "A")
    bhv.add_transition("C", "A", lambda r: r == "A")
    bhv.add_transition("C", "B", lambda r: r == "B")
    bhv.add_transition("B", "D", lambda r: r == "D")

    return bhv


def counter(result):
    yield from result


async def test_behavior_fsm():
    result = "BABACBD#"
    out = []
    c = counter(result)
    bhv = get_fsm(out, c)
    await bhv.run()

    assert "".join(out) == result


async def test_wrong_fsm_behavior():
    result = "BABCCBD#"
    out = []
    c = counter(result)
    bhv = get_fsm(out, c)

    with pytest.raises(Exception):
        await bhv.run()


async def test_suicide_behavior():
    bhv = piaf.behavior.SuicideBehavior(agent)
    await bhv.run()

    assert agent.has_quit
