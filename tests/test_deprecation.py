# coding: utf-8
import importlib

from pytest import fail
from semver import VersionInfo

import piaf


def test_deprecated_platform_module():
    """Ensure the platform module no longer exists starting with piaf 1.0."""
    current_version = VersionInfo.parse(piaf.__version__)
    if current_version.major >= 1:
        try:
            importlib.import_module("piaf.platform")
            fail("Module 'piaf.platform' still exists.")
        except ModuleNotFoundError:
            pass  # Success
