import asyncio
import logging

import pytest
from piaf.agent import Agent
from piaf.comm import AID
from piaf.exceptions import InvalidStateError
from piaf.launcher import AgentDescription, PlatformLauncher, ServiceDescription
from piaf.ptf import AgentPlatform, AgentPlatformFacade
from piaf.service import DFService


class AgentWithArgsAndKwargs(Agent):
    def __init__(
        self, aid: AID, platform: AgentPlatformFacade, arg: str, *, kwarg: int = 0
    ):
        super().__init__(aid, platform)
        self.logger.info("arg: %s; kwarg: %s", arg, kwarg)


@pytest.fixture()
def loop() -> asyncio.AbstractEventLoop:
    try:
        return asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.get_event_loop_policy().new_event_loop()
        asyncio.set_event_loop(loop)
        return loop


async def hack_platform_stop(ptf: AgentPlatform, timeout: float = 1) -> None:
    await asyncio.sleep(timeout)
    await ptf.stop()


class TestPlatformLauncher:
    def test_add_agent_ok(self, caplog, loop: asyncio.AbstractEventLoop) -> None:
        launcher = PlatformLauncher("test")
        launcher.add_agent(
            AgentDescription("agent", AgentWithArgsAndKwargs, ("arg",), {"kwarg": 5})
        )

        loop.create_task(hack_platform_stop(launcher.ptf))
        with caplog.at_level(logging.INFO):
            launcher.run()

        assert "arg: arg; kwarg: 5" in caplog.messages

    def test_add_agent_wrong_state(self, loop: asyncio.AbstractEventLoop) -> None:
        launcher = PlatformLauncher("test")

        async def add_agent() -> None:
            await asyncio.sleep(0.5)
            with pytest.raises(InvalidStateError):
                launcher.add_agent(AgentDescription("agent", Agent))

        loop.create_task(hack_platform_stop(launcher.ptf))
        loop.create_task(add_agent())
        launcher.run()

    def test_add_service_ok(self, caplog, loop: asyncio.AbstractEventLoop) -> None:
        launcher = PlatformLauncher("test")
        launcher.add_service(ServiceDescription("df", DFService))

        loop.create_task(hack_platform_stop(launcher.ptf))
        with caplog.at_level(logging.INFO):
            launcher.run()

        assert "Please welcome Agent df !" in caplog.messages

    def test_add_service_wrong_state(self, loop: asyncio.AbstractEventLoop) -> None:
        launcher = PlatformLauncher("test")

        async def add_service() -> None:
            await asyncio.sleep(0.5)
            with pytest.raises(InvalidStateError):
                launcher.add_service(ServiceDescription("df", DFService))

        loop.create_task(hack_platform_stop(launcher.ptf))
        loop.create_task(add_service())
        launcher.run()
