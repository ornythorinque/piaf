import pytest


def test_raises_depracted_warning():
    with pytest.warns(DeprecationWarning):
        from piaf.platform import AgentPlatform  # noqa
