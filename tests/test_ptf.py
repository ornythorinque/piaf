from __future__ import annotations

import asyncio
from typing import Sequence

import piaf.agent
import piaf.comm
import piaf.comm.mts
import piaf.exceptions as ex
import piaf.ptf as plt
import pytest
from piaf.audit import AGENTS_PARENT_TOPIC, PLATFORM_TOPIC, Event, EventManager, Topic

from tests.test_audit import DummySubscriber


class AgentMock:
    def __init__(self, aid, platform, *args, **kwargs):
        self.aid = aid
        self.platform = platform
        self.args = args
        self.kwargs = kwargs
        self._has_run = asyncio.Event()
        self.mailbox = piaf.comm.mts.MailBox()

    async def run(self):
        await self._has_run.set()

    async def has_run(self):
        await self._has_run.wait()
        return True


class ACCMock:
    """Type mocking the real Agent Communication Channel."""

    def __init__(self, ptf: PlatformMock) -> None:
        """Create a new ACCMock instance."""
        self._ptf = ptf

    @property
    def schemes(self) -> tuple[str, ...]:
        """Get the list of supported schemes."""
        return ("http", "ftp")

    def register_agent_or_service(
        self, name: str, mailbox: piaf.comm.mts.MailBox
    ) -> None:
        """Register an agent or a service to this ACC."""

    def forget_agent_or_service(self, name: str) -> None:
        """Forget an agent or a service."""

    @property
    def addresses(self):
        """Get all addresses this ACC can be contacted with."""
        return (f"{scheme}://{self._ptf.name}/acc" for scheme in self.schemes)


class PlatformMock:
    def __init__(self) -> None:
        self.acc: ACCMock = ACCMock(self)
        self.state = plt.PlatformState.RUNNING
        self.name = "localhost"
        self.evt_manager = EventManager()

    @property
    def schemes(self) -> Sequence[str]:
        return self.acc.schemes


class TestAgentManager:
    """Gather all tests related to AgentManager."""

    @pytest.fixture()
    def am(self):
        """Get a fresh new AgentPlatform."""
        ptf = PlatformMock()
        return plt.AgentManager(ptf, ptf.acc)

    async def test_create(self, am: plt.AgentManager):
        """
        Test :meth:`AgentManager.create` method.

        Expected result is:
         - agent creation
         - addresses = acc addresses
         - agent state is INITIATED
         - duplicated agent name is not allowed
         - platform state must be 'STARTED'
        """
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create agent
        aid = await am.create(AgentMock, "test")

        # Assert basic stuff about agent
        assert aid.short_name == "test"
        assert list(aid.addresses) == list(am._acc.addresses)
        assert am.get_state(aid) == piaf.agent.AgentState.INITIATED

        # Ensure an event is fired
        sub.records[0].event == Event(
            aid.name, "agent_creation", piaf.comm.AID("test@localhost", [], [])
        )

        # Duplicated name
        with pytest.raises(ex.DuplicatedNameError):
            await am.create(AgentMock, "test")

        # Invalid platform state
        with pytest.raises(ex.InvalidStateError):
            am._platform.state = plt.PlatformState.STOPPED
            await am.create(AgentMock, "test2")

    async def test_wait(self, am: plt.AgentManager):
        """Check wait method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create agent and invoke it
        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)

        await am.wait(aid)
        assert am.get_state(aid) == piaf.agent.AgentState.WAITING

        # Ensure an event is fired
        assert Event(
            aid.name,
            "state_change",
            {
                "from": piaf.agent.AgentState.ACTIVE,
                "to": piaf.agent.AgentState.WAITING,
            },
        ) in (r.event for r in sub.records)

        with pytest.raises(ex.StateTransitionError):
            await am.wait(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.invoke(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.suspend(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.resume(aid)

    async def test_wake_up(self, am: plt.AgentManager) -> None:
        """Check wake_up method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create agent and invoke it, then wait
        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)
        await am.wait(aid)

        await am.wake_up(aid)
        assert am.get_state(aid) == piaf.agent.AgentState.ACTIVE

        # Ensure an event is fired
        assert Event(
            aid.name,
            "state_change",
            {
                "from": piaf.agent.AgentState.WAITING,
                "to": piaf.agent.AgentState.ACTIVE,
            },
        ) in (r.event for r in sub.records)

        with pytest.raises(ex.StateTransitionError):
            await am.wake_up(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.invoke(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.resume(aid)

    async def test_suspend(self, am: plt.AgentManager) -> None:
        """Check suspend method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create agent and invoke it
        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)

        await am.suspend(aid)
        assert am.get_state(aid) == piaf.agent.AgentState.SUSPENDED

        # Ensure an event is fired
        assert Event(
            aid.name,
            "state_change",
            {
                "from": piaf.agent.AgentState.ACTIVE,
                "to": piaf.agent.AgentState.SUSPENDED,
            },
        ) in (r.event for r in sub.records)

        with pytest.raises(ex.StateTransitionError):
            await am.suspend(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.wake_up(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.invoke(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.wait(aid)

    async def test_resume(self, am: plt.AgentManager) -> None:
        """Check resume method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create agent and invoke it, then suspend
        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)
        await am.suspend(aid)

        await am.resume(aid)
        assert am.get_state(aid) == piaf.agent.AgentState.ACTIVE

        # Ensure an event is fired
        assert Event(
            aid.name,
            "state_change",
            {
                "from": piaf.agent.AgentState.SUSPENDED,
                "to": piaf.agent.AgentState.ACTIVE,
            },
        ) in (r.event for r in sub.records)

        with pytest.raises(ex.StateTransitionError):
            await am.resume(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.wake_up(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.invoke(aid)

    async def test_invoke(self, am: plt.AgentManager) -> None:
        """Test invoke method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        aid = await am.create(AgentMock, "test")

        await am.invoke(aid)
        assert am.get_state(aid) == piaf.agent.AgentState.ACTIVE

        # Ensure an event is fired
        assert Event(
            aid.name,
            "state_change",
            {
                "from": piaf.agent.AgentState.INITIATED,
                "to": piaf.agent.AgentState.ACTIVE,
            },
        ) in (r.event for r in sub.records)

        with pytest.raises(ex.StateTransitionError):
            await am.resume(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.wake_up(aid)

        with pytest.raises(ex.StateTransitionError):
            await am.invoke(aid)

        assert await am._contexts[aid].agent.has_run()

    async def test_quit(self, am: plt.AgentManager) -> None:
        """Test quit method on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(
            sub, Topic.resolve_from_parent_topic(AGENTS_PARENT_TOPIC, "test")
        )

        # Create and invoke agent
        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)

        # Quit ASAP
        await am.quit(aid)

        await asyncio.sleep(0.2)
        assert am._contexts.get(aid) is None
        assert am.get_state(aid) == piaf.agent.AgentState.UNKNOWN

        assert Event(aid.name, "agent_death", None) in (r.event for r in sub.records)

        with pytest.raises(ex.IllegalArgumentError):
            await am.quit(aid)

        with pytest.raises(ex.IllegalArgumentError):
            await am.resume(aid)

        with pytest.raises(ex.IllegalArgumentError):
            await am.wake_up(aid)

        with pytest.raises(ex.IllegalArgumentError):
            await am.wait(aid)

        with pytest.raises(ex.IllegalArgumentError):
            await am.suspend(aid)

        with pytest.raises(ex.IllegalArgumentError):
            await am.invoke(aid)

    async def test_stop_all(self, am: plt.AgentManager) -> None:
        """Test method stop_all on AgentManager."""
        # Listen to events
        sub = DummySubscriber()
        am._platform.evt_manager.subscribe_to(sub, AGENTS_PARENT_TOPIC)

        aid = await am.create(AgentMock, "test")
        await am.invoke(aid)

        aid = await am.create(AgentMock, "test2")
        await am.invoke(aid)

        aid = await am.create(AgentMock, "test3")
        await am.invoke(aid)

        await asyncio.sleep(0.1)
        await am.stop_all()

        assert len(am._contexts) == 0

        # Ensure events are fired and propagated
        records = [r.event for r in sub.records]
        assert Event("test@localhost", "agent_death", None) in records
        assert Event("test2@localhost", "agent_death", None) in records
        assert Event("test3@localhost", "agent_death", None) in records


class TestAgentPlatform:
    """Gather all tests related to the :class:`AgentPlatform`."""

    PTF_NAME = "localhost"

    @pytest.fixture()
    def ap(self) -> plt.AgentPlatform:
        """Get a new instance of :class:`AgentPlatform`."""
        return plt.AgentPlatform(self.PTF_NAME)

    async def test_stop_platform_agents_paused(
        self, ap: plt.AgentPlatform, caplog
    ) -> None:
        """
        Pause all agents and then stop the platform.

        This test ensures that agents are quitting properly even if their are paused.
        Non-regression test for issue https://gitlab.com/ornythorinque/piaf/-/issues/4.

        :param ap: agent platform instance
        :param caplog: pytest fixture to manipulate emitted logs
        """
        await ap.start()

        # Hack ams to pause it
        for aid in ap.agent_manager._contexts:
            await ap.agent_manager.suspend(aid)

        # Stop platform
        await ap.stop()

        for record in caplog.records:
            assert "raised an exception" not in record.text

    async def test_start_platform(self, ap: plt.AgentPlatform) -> None:
        """Ensure the platform is started correctly."""
        # Subscribe to events emitted by the platform
        em: EventManager = ap.evt_manager
        sub = DummySubscriber()
        em.subscribe_to(sub, PLATFORM_TOPIC)

        # Start the platform
        await ap.start()

        # Filter records
        records = [r for r in sub.records if r.event.source == "platform"]

        # Asserts
        assert ap.state == plt.PlatformState.RUNNING
        assert len(records) == 1
        assert records[0].topics == (PLATFORM_TOPIC,)
        assert records[0].event.type == "state_change"
        assert records[0].event.data == {
            "from": plt.PlatformState.INITIALIZED,
            "to": plt.PlatformState.RUNNING,
        }

        # Can't start the platform twice
        with pytest.raises(ex.StateTransitionError):
            await ap.start()

        await ap.stop()

        # Can't start a dead platform
        with pytest.raises(ex.StateTransitionError):
            await ap.start()

    async def test_stop_platform(self, ap: plt.AgentPlatform) -> None:
        """Ensure the platform is stopped correctly."""
        # Can't stop an initialized platform
        with pytest.raises(ex.StateTransitionError):
            await ap.stop()

        # Start the platform
        await ap.start()

        # Subscribe to events emitted by the platform
        em: EventManager = ap.evt_manager
        sub = DummySubscriber()
        em.subscribe_to(sub, PLATFORM_TOPIC)

        # Stop the platform
        await ap.stop()
        await em.close()

        # Ensure state is correct
        assert ap.state == plt.PlatformState.STOPPED

        # Filter records and check if the platform emitted an event
        records = [r for r in sub.records if r.event.source == "platform"]
        assert len(records) == 1
        assert records[0].topics == (PLATFORM_TOPIC,)
        assert records[0].event.type == "state_change"
        assert records[0].event.data == {
            "from": plt.PlatformState.RUNNING,
            "to": plt.PlatformState.STOPPED,
        }

        # Can't stop a platform twice
        with pytest.raises(ex.StateTransitionError):
            await ap.stop()

    def test_initialization(self) -> None:
        """Ensure the platform is correctly created."""
        # Create platform
        ap = plt.AgentPlatform(self.PTF_NAME)

        assert ap.name == self.PTF_NAME
        assert ap.state == plt.PlatformState.INITIALIZED


class TestAgentPlatformFacade:
    """Gather all tests related to the :class:`AgentPlatform`."""

    PTF_NAME = "localhost"

    @pytest.fixture()
    def ap(self) -> plt.AgentPlatform:
        """Get a new instance of :class:`AgentPlatform`."""
        return plt.AgentPlatform(self.PTF_NAME)

    @pytest.fixture()
    def sext(self) -> plt.Extension:
        class ServiceExtension(plt.Extension):
            @property
            def require_service(self):
                return True

        return ServiceExtension()

    @pytest.fixture()
    def ext(self) -> plt.Extension:
        return plt.Extension()

    async def test_extensions(
        self, ap: plt.AgentPlatform, ext: plt.Extension, sext: plt.Extension
    ) -> None:
        """
        Ensure that if the agent is a service, then it has access to all extensions
        and if not, only regular extensions are listed.
        """
        ap.load_extension("ext", ext)
        ap.load_extension("sext", sext)

        await ap.start()

        facade = plt.AgentPlatformFacade(ap)
        assert ("ext", ext) in facade.extensions.items()
        assert ("sext", sext) not in facade.extensions.items()

        facade = plt.AgentPlatformFacade(ap, is_service=True)
        assert ("ext", ext) in facade.extensions.items()
        assert ("sext", sext) in facade.extensions.items()

        await ap.stop()
