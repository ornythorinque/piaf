from __future__ import annotations

import asyncio

import piaf.service
import pytest
from piaf.agent import Agent, AgentState
from piaf.behavior import Behavior
from piaf.comm import AID, ACLMessage, Message, Performative
from piaf.ptf import AgentPlatform

PTL_NAME = "localhost"
AMS_ADDR = AID("ams@localhost")
DF_ADDR = AID("df@localhost")
OTHER_ADDR = AID("other@localhost")


@pytest.fixture()
def ap():
    """Get a fresh new AgentPlatform."""
    return AgentPlatform(PTL_NAME)


class SendBehavior(Behavior):
    """
    Behavior sending messages.

    Using a queue provided, this behavior will get messages and send them.
    """

    def __init__(self, agent: piaf.agent.Agent, queue: asyncio.Queue):
        """
        Build a new class:`SendBehavior` object.

        :param msg: the message queue
        :param agent: the agent owner of this behavior
        """
        super().__init__(agent)
        self.queue = queue

    def done(self) -> bool:
        """Infinite behavior, returns always `False`."""
        return False

    async def action(self) -> None:
        """Each time a message is put in the queue, get it and send it."""
        self.agent.send(await self.queue.get())
        self.queue.task_done()


class ReceiveBehavior(Behavior):
    """
    Behavior receiving messages.

    It will put received messages in the provided queue.
    """

    def __init__(self, agent: piaf.agent.Agent, queue: asyncio.Queue):
        """
        Build a new :class:`ReceiveBehavior` object.

        :param agent: owner of this behavior
        :param queue: where to put messages
        """
        super().__init__(agent)
        self.queue = queue

    def done(self) -> bool:
        """Infinite behavior, returns always `False`."""
        return False

    async def action(self) -> None:
        """Each time a message is received, put it in the queue."""
        await self.queue.put(await self.agent.receive())


class HelperAgent(Agent):
    """Helper agent for sending and receiving messages."""

    def __init__(
        self,
        aid: piaf.comm.AID,
        platform: piaf.ptf.AgentPlatformFacade | piaf.ptf.AgentPlatform,
        queue_in: asyncio.Queue,
        queue_out: asyncio.Queue,
    ):
        """
        Build a new :class:`HelperAgent` object.

        :param aid: this agent identifier
        :param platform: the agent platform
        :param queue_in: queue containing :class:`ACLMessage` to send
        :param queue_out: queue where received messages (:class:`Message`) will be put
        """
        super().__init__(aid, platform)
        self.add_behavior(SendBehavior(self, queue_in))
        self.add_behavior(ReceiveBehavior(self, queue_out))


class TestBaseService:
    """All tests common to both the AMS and the DF."""

    async def test_pre_content_check(self, ap: AgentPlatform):
        """
        All kind of tests to check operations done before calling the requested function.

        Will test:

        * Unsupported act: act other than REQUEST and CANCEL
        * Unsupported value: content has the wrong type
        * Missing parameter: no function name provided
        * Unrecognized parameter value: function name is not a string
        * Unsupported function: wrong function name

        """
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )

        await ap.agent_manager.invoke(helper_aid)

        # Send message with performative != REQUEST
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REJECT_PROPOSAL)
            .receiver(AMS_ADDR)
            .conversation_id("rejected")
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply: Message = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert reply.acl_message.content[0] == acl_message
        assert (
            reply.acl_message.content[1]
            == f"Unsupported Act: {acl_message.performative}"
        )
        assert reply.acl_message.performative == Performative.NOT_UNDERSTOOD

        # Send message with performative == CANCEL but wrong time
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.CANCEL)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_cancel")
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply: Message = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert reply.acl_message.content[0] == acl_message
        assert (
            reply.acl_message.content[1]
            == f"Unexpected Act: {acl_message.performative}"
        )
        assert reply.acl_message.performative == Performative.NOT_UNDERSTOOD

        # Send message with wrong content type
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_content_type")
            .content("Wrong content type")
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.performative == Performative.NOT_UNDERSTOOD
        assert reply.acl_message.content[0] == acl_message
        assert reply.acl_message.content[1] == "Unsupported value: content"

        # Send message with empty sequence as content
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("empty_sequence")
            .content([])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert reply.acl_message.content[1] == "Missing parameter: function_name"

        # Send message with wrong type for function_name
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_function_name_type")
            .content([3])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert (
            reply.acl_message.content[1]
            == f"Unrecognized parameter value: function_name, {acl_message.content[0]}"
        )

        # Send message with non-existing function
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_search_constraints_type")
            .content(["Does not exists"])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert (
            reply.acl_message.content[1]
            == f"Unsupported function: {acl_message.content[0]}"
        )

        await ap.stop()


class TestAMSService:
    """All tests related to the AMS agent."""

    async def test_search(self, ap: AgentPlatform):
        """Run a search query and test failure case."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )

        await ap.agent_manager.invoke(helper_aid)

        # Send message with a valid description but no constraints
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("search")
            .content(
                [
                    piaf.service.AMSService.SEARCH_FUNC,
                    piaf.service.AMSAgentDescription(None, None, AgentState.ACTIVE),
                ]
            )
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply (AGREE + INFORM)
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.content == acl_message.content

        reply: Message = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert len(reply.acl_message.content[1]) == 2

        aids = [aad.name for aad in reply.acl_message.content[1]]
        assert AMS_ADDR in aids
        assert OTHER_ADDR in aids

        # Send message with wrong number of arguments
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_function_name_type")
            .content([piaf.service.AMSService.SEARCH_FUNC])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert reply.acl_message.content[1] == "Missing argument: ams-agent-description"

        # Send message with wrong number of arguments (too many)
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_function_name_type")
            .content([piaf.service.AMSService.SEARCH_FUNC, "titi", "tata", "toto"])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert reply.acl_message.content[1] == "Unexpected argument count"

        # Send message with wrong ams-agent-description argument type
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_ams-agent-description_type")
            .content([piaf.service.AMSService.SEARCH_FUNC, "test"])
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert (
            reply.acl_message.content[1]
            == f"Unrecognised parameter value: ams-agent-description, {acl_message.content[1]}"
        )

        # Send message with wrong search-constraints argument type
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong_ams-agent-description_type")
            .content(
                [
                    piaf.service.AMSService.SEARCH_FUNC,
                    piaf.service.AMSAgentDescription(None, None, AgentState.ACTIVE),
                    "test",
                ]
            )
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply
        reply = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.performative == Performative.REFUSE
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert (
            reply.acl_message.content[1]
            == f"Unrecognised parameter value: search-constraints, {acl_message.content[2]}"
        )

        # Search and constrain how many results we can get
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("search_constrained")
            .content(
                [
                    piaf.service.AMSService.SEARCH_FUNC,
                    piaf.service.AMSAgentDescription(None, None, AgentState.ACTIVE),
                    piaf.service.SearchConstraints(max_results=1),
                ]
            )
            .build()
        )
        await queue_in.put(acl_message)

        # Check AMS reply (AGREE + INFORM)
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.content == acl_message.content

        reply: Message = await queue_out.get()
        queue_out.task_done()

        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert len(reply.acl_message.content) == 2
        assert reply.acl_message.content[0] == acl_message.content
        assert len(reply.acl_message.content[1]) == 1

        aids = [aad.name for aad in reply.acl_message.content[1]]
        assert AMS_ADDR in aids or OTHER_ADDR in aids

        await ap.stop()

    async def test_create_agent(self, ap: AgentPlatform) -> None:
        """Test the creation of an agent using the AMS."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )

        await ap.agent_manager.invoke(helper_aid)

        # Missing mandatory argument
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("missing-agent-creation-description-parameter")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert (
            resp.acl_message.content[1]
            == "Missing argument: agent-creation-description"
        )

        # Too many arguments
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("too-many-arguments")
            .content([piaf.service.AMSService.CREATE_AGENT_FUNC, "foo", "bar"])
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Unexpected argument count"

        # Wrong argument type
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("wrong-parameter-type")
            .content([piaf.service.AMSService.CREATE_AGENT_FUNC, "wrong-type"])
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert (
            resp.acl_message.content[1]
            == f"Unrecognised parameter value: agent-creation-description, {req.content[1]}"
        )

        # No module provided
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("no-module")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription("NoModule", "no-module"),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "No module provided."

        # Unknow module
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("unknown-module")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription(
                        "unknown.module.UnknownClass", "unknown-class"
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "No module named 'unknown.module'"

        # Unknow class
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("unknown-class")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription(
                        "piaf.agent.UnknownClass", "unknown-class"
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert (
            resp.acl_message.content[1]
            == "No agent class named 'UnknownClass' in module 'piaf.agent'"
        )

        # Unable to instantiate
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("instantiation-error")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription(
                        "piaf.agent.Agent", "testing", ["arg"]
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert "Internal error" in resp.acl_message.content[1]

        # Duplicate short name
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("duplicate-short-name")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription("piaf.agent.Agent", "ams"),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Duplicated name: ams"

        # Success (instantiate the DFService)
        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("success")
            .content(
                [
                    piaf.service.AMSService.CREATE_AGENT_FUNC,
                    piaf.service.AgentCreationDescription(
                        "piaf.service.DFService", "df", is_service=True
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.INFORM
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == AID("df@localhost", [], [])

        req: ACLMessage = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .receiver(AMS_ADDR)
            .conversation_id("query-success")
            .content(
                [
                    piaf.service.AMSService.SEARCH_FUNC,
                    piaf.service.AMSAgentDescription(state=AgentState.ACTIVE),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.INFORM
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content[0] == req.content
        assert AID("df@localhost", [], []) in [
            description.name for description in resp.acl_message.content[1]
        ]

        # Stop the platform
        await ap.stop()

    async def test_modify(self, ap: AgentPlatform) -> None:
        """Test the modification of an agent's registration."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )

        await ap.agent_manager.invoke(helper_aid)

        # No argument supplied
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("no-argument")
            .receiver(AMS_ADDR)
            .content([piaf.service.AMSService.MODIFY_FUNC])
            .build()
        )
        await queue_in.put(req)

        # Expect a REFUSE from the AMS telling the ams-agent-description is missing
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Missing parameter: ams-agent-description"

        # Too many arguments
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("too-many-arguments")
            .receiver(AMS_ADDR)
            .content([piaf.service.AMSService.MODIFY_FUNC, "arg1", "arg2"])
            .build()
        )
        await queue_in.put(req)

        # Expect a REFUSE from the AMS telling there are too many arguments
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Unexpected argument count"

        # Wrong argument type
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("wrong-argument-type")
            .receiver(AMS_ADDR)
            .content([piaf.service.AMSService.MODIFY_FUNC, "arg1"])
            .build()
        )
        await queue_in.put(req)

        # Expect a REFUSE from the AMS telling the argument type is wrong
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.REFUSE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[0] == req.content
        assert (
            resp.acl_message.content[1]
            == f"Unrecognised parameter value: agent-creation-description, {req.content[1]}"
        )

        # This feature is deactivated because otherwise the modify function has very little interest.
        # Once the ownership is implemented, it could become interesting to limit agent state modification to both the agent itself and its owner

        # Trying to change the state of another agent
        # req = (
        #     ACLMessage.Builder()
        #     .performative(Performative.REQUEST)
        #     .conversation_id("unauthorized")
        #     .receiver(AMS_ADDR)
        #     .content(
        #         [
        #             piaf.service.AMSService.MODIFY_FUNC,
        #             piaf.service.AMSAgentDescription(
        #                 name=AMS_ADDR, state=AgentState.SUSPENDED
        #             ),
        #         ]
        #     )
        #     .build()
        # )
        # await queue_in.put(req)

        # # Expect an AGREE from the AMS
        # resp: Message = await queue_out.get()
        # queue_out.task_done()

        # assert resp.acl_message.performative == Performative.AGREE
        # assert resp.acl_message.conversation_id == req.conversation_id
        # assert resp.acl_message.sender == AMS_ADDR
        # assert resp.acl_message.content == req.content

        # # Expect a failure message from the AMS telling the agent is unauthorized
        # resp: Message = await queue_out.get()
        # queue_out.task_done()

        # assert resp.acl_message.performative == Performative.FAILURE
        # assert resp.acl_message.conversation_id == req.conversation_id
        # assert resp.acl_message.sender == AMS_ADDR
        # assert len(resp.acl_message.content) == 2
        # assert resp.acl_message.content[0] == req.content
        # assert resp.acl_message.content[1] == "Unauthorized"

        # Try to change the owner, which is unsupported
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("unsupported")
            .receiver(AMS_ADDR)
            .content(
                [
                    piaf.service.AMSService.MODIFY_FUNC,
                    piaf.service.AMSAgentDescription(name=OTHER_ADDR, ownership="me"),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        # Expect an AGREE from the AMS
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content == req.content

        # Expect a failure message from the AMS telling changing the ownership is unsupported
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Unsupported: ownership"

        # Try to perform an invalid state change (ACTIVE to WAITING)
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("valid-state-change")
            .receiver(AMS_ADDR)
            .content(
                [
                    piaf.service.AMSService.MODIFY_FUNC,
                    piaf.service.AMSAgentDescription(
                        name=OTHER_ADDR, state=AgentState.WAITING
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        # Expect an AGREE from the AMS
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content == req.content

        # Expect an inform message from the AMS telling the change can't be made
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.FAILURE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[0] == req.content
        assert resp.acl_message.content[1] == "Unsupported state: AgentState.WAITING"

        # Try to perform a valid state change (ACTIVE to SUSPENDED)
        aid = await ap.agent_manager.create(
            HelperAgent, "test", asyncio.Queue(), asyncio.Queue(), is_service=False
        )
        await ap.agent_manager.invoke(aid)

        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("valid-state-change")
            .receiver(AMS_ADDR)
            .content(
                [
                    piaf.service.AMSService.MODIFY_FUNC,
                    piaf.service.AMSAgentDescription(
                        name=aid, state=AgentState.SUSPENDED
                    ),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        # Expect an AGREE from the AMS
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.AGREE
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content == req.content

        # Expect an inform message from the AMS telling the change has been performed
        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert resp.acl_message.performative == Performative.INFORM
        assert resp.acl_message.conversation_id == req.conversation_id
        assert resp.acl_message.sender == AMS_ADDR
        assert resp.acl_message.content == req.content

        # Check state
        req = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("check-state")
            .receiver(AMS_ADDR)
            .content(
                [
                    piaf.service.AMSService.SEARCH_FUNC,
                    piaf.service.AMSAgentDescription(name=aid),
                ]
            )
            .build()
        )
        await queue_in.put(req)

        resp: Message = await queue_out.get()
        queue_out.task_done()

        resp: Message = await queue_out.get()
        queue_out.task_done()

        assert len(resp.acl_message.content) == 2
        assert resp.acl_message.content[1][0].state == AgentState.SUSPENDED

        await ap.stop()
        await ap.evt_manager.close()


class TestDFService:
    """All tests related to the DF service."""

    async def test_register(self, ap: AgentPlatform):
        """Test success and failure cases of the 'register' function."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )
        await ap.agent_manager.invoke(helper_aid)

        # Create and invoke the DF service
        await ap.agent_manager.create(
            piaf.service.DFService, DF_ADDR.short_name, is_service=True
        )
        await ap.agent_manager.invoke(DF_ADDR)

        # Create register message with wrong arity
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content([piaf.service.DFService.REGISTER_FUNC])
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Make sure the DFService replies with a REFUSE
        refuse: Message = await queue_out.get()
        queue_out.task_done()

        assert refuse.acl_message.performative == Performative.REFUSE
        assert refuse.acl_message.sender == DF_ADDR
        assert refuse.acl_message.conversation_id == acl_message.conversation_id
        assert refuse.acl_message.content[0] == acl_message.content
        assert refuse.acl_message.content[1] == "Missing argument: df agent description"

        # Create register message with wrong argument type
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content([piaf.service.DFService.REGISTER_FUNC, "toto"])
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Make sure the DFService replies with a REFUSE
        refuse: Message = await queue_out.get()
        queue_out.task_done()

        assert refuse.acl_message.performative == Performative.REFUSE
        assert refuse.acl_message.sender == DF_ADDR
        assert refuse.acl_message.conversation_id == acl_message.conversation_id
        assert refuse.acl_message.content[0] == acl_message.content
        assert (
            refuse.acl_message.content[1]
            == f"Unrecognised parameter value: df agent description, {acl_message.content[1]}"
        )

        # Valid registration request
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content(
                [
                    piaf.service.DFService.REGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Ensure response is AGREE then INFORM
        agree: Message = await queue_out.get()
        queue_out.task_done()
        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == acl_message.content

        reply: Message = await queue_out.get()
        queue_out.task_done()
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert reply.acl_message.performative == Performative.INFORM
        assert reply.acl_message.sender == DF_ADDR
        assert reply.acl_message.content == acl_message.content

        # Try to register one more time, should fail
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content(
                [
                    piaf.service.DFService.REGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Agree because message is well formed
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.content == acl_message.content

        # But fails because the agent is already registered
        failure: Message = await queue_out.get()
        queue_out.task_done()

        assert failure.acl_message.performative == Performative.FAILURE
        assert failure.acl_message.sender == DF_ADDR
        assert failure.acl_message.conversation_id == acl_message.conversation_id
        assert failure.acl_message.content[0] == acl_message.content
        assert failure.acl_message.content[1] == "Already registered"

        # Stop the platform
        await ap.stop()

    async def test_deregister(self, ap: AgentPlatform):
        """Test success and failure cases of the 'deregister' function."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )
        await ap.agent_manager.invoke(helper_aid)

        # Create and invoke the DF service
        await ap.agent_manager.create(
            piaf.service.DFService, DF_ADDR.short_name, is_service=True
        )
        await ap.agent_manager.invoke(DF_ADDR)

        # Create register message with wrong arity
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("deregister")
            .content([piaf.service.DFService.DEREGISTER_FUNC])
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Make sure the DFService replies with a REFUSE
        refuse: Message = await queue_out.get()

        assert refuse.acl_message.performative == Performative.REFUSE
        assert refuse.acl_message.sender == DF_ADDR
        assert refuse.acl_message.conversation_id == acl_message.conversation_id
        assert refuse.acl_message.content[0] == acl_message.content
        assert refuse.acl_message.content[1] == "Missing argument: df agent description"

        # Create register message with wrong argument type
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("deregister")
            .content([piaf.service.DFService.DEREGISTER_FUNC, "toto"])
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Make sure the DFService replies with a REFUSE
        refuse: Message = await queue_out.get()

        assert refuse.acl_message.performative == Performative.REFUSE
        assert refuse.acl_message.sender == DF_ADDR
        assert refuse.acl_message.conversation_id == acl_message.conversation_id
        assert refuse.acl_message.content[0] == acl_message.content
        assert (
            refuse.acl_message.content[1]
            == f"Unrecognised parameter value: df agent description, {acl_message.content[1]}"
        )

        # Valid registration request
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content(
                [
                    piaf.service.DFService.REGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Consume registration agreement and inform
        _: Message = await queue_out.get()
        queue_out.task_done()
        _: Message = await queue_out.get()
        queue_out.task_done()

        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("deregister")
            .content(
                [
                    piaf.service.DFService.DEREGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Ensure response is AGREE then INFORM
        agree: Message = await queue_out.get()
        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == acl_message.content

        reply: Message = await queue_out.get()
        assert reply.acl_message.conversation_id == acl_message.conversation_id
        assert reply.acl_message.performative == Performative.INFORM
        assert reply.acl_message.sender == DF_ADDR
        assert reply.acl_message.content == acl_message.content

        # Try to deregister one more time, should fail
        acl_message = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .content(
                [
                    piaf.service.DFService.DEREGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .receiver(DF_ADDR)
            .build()
        )
        await queue_in.put(acl_message)

        # Agree because message is correct
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == acl_message.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == acl_message.content

        # Then fail because DF has no record about the agent
        failure: Message = await queue_out.get()
        queue_out.task_done()

        assert failure.acl_message.performative == Performative.FAILURE
        assert failure.acl_message.sender == DF_ADDR
        assert failure.acl_message.conversation_id == acl_message.conversation_id
        assert failure.acl_message.content[0] == acl_message.content
        assert failure.acl_message.content[1] == "Not registered"

        # Stop the platform
        await ap.stop()

    async def test_search(self, ap: AgentPlatform) -> None:
        """Test success and failure cases of the 'deregister' function."""
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )
        await ap.agent_manager.invoke(helper_aid)

        # Create and invoke the DF service
        await ap.agent_manager.create(
            piaf.service.DFService, DF_ADDR.short_name, is_service=True
        )
        await ap.agent_manager.invoke(DF_ADDR)

        # Search with wrong argument number (too low)
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("search_wrong_arg_number")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.SEARCH_FUNC])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Missing argument: df-agent-description",
        ]
        queue_out.task_done()

        # Search with wrong argument number (too high)
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("search_wrong_arg_number")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.SEARCH_FUNC, "titi", "toto", "tata"])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Unexpected argument count",
        ]
        queue_out.task_done()

        # Search with wrong argument type (df-agent-description)
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("search_wrong_arg_type")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.SEARCH_FUNC, "titi", "toto"])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Unrecognised parameter value: df-agent-description, titi",
        ]
        queue_out.task_done()

        # Search with wrong argument type (search-constraints)
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("search_wrong_arg_type")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.SEARCH_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                    "toto",
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Unrecognised parameter value: search-constraints, toto",
        ]
        queue_out.task_done()

        # Search with no results
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("no_result")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.SEARCH_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a agree then inform with empty content
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.AGREE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        queue_out.task_done()

        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.INFORM
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            [],
        ]
        queue_out.task_done()

        # Register service description
        register = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.REGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(register)

        # Consume response
        response = await queue_out.get()  # Agree
        queue_out.task_done()
        response = await queue_out.get()  # Inform
        queue_out.task_done()

        # Search with one result
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("one_result")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.SEARCH_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a agree then inform with empty content
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.AGREE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        queue_out.task_done()

        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.INFORM
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            [piaf.service.DFAgentDescription(helper_aid)],
        ]
        queue_out.task_done()

        # Search with max-result constraint
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("one_result")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.SEARCH_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                    piaf.service.SearchConstraints(max_results=0),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a agree then inform with empty content
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.AGREE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        queue_out.task_done()

        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.INFORM
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            [],
        ]
        queue_out.task_done()
        await ap.stop()

    async def test_modify(self, ap: AgentPlatform) -> None:
        """
        Test success and failure cases for the modify function.

        :param ap: a working agent platform
        """
        # Launch the platform
        await ap.start()

        # Create an invoke the helper agent
        queue_in = asyncio.Queue()
        queue_out = asyncio.Queue()
        helper_aid = await ap.agent_manager.create(
            HelperAgent, OTHER_ADDR.short_name, queue_in, queue_out, is_service=False
        )
        await ap.agent_manager.invoke(helper_aid)

        # Create and invoke the DF service
        await ap.agent_manager.create(
            piaf.service.DFService, DF_ADDR.short_name, is_service=True
        )
        await ap.agent_manager.invoke(DF_ADDR)

        # Missing df-agent-description-parameter
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("non-existing-record")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.MODIFY_FUNC])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Missing argument: df-agent-description",
        ]
        queue_out.task_done()

        # Wrong df-agent-description type
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("modify_wrong_arg_type")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.MODIFY_FUNC, "titi"])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Unrecognised parameter value: df-agent-description, titi",
        ]
        queue_out.task_done()

        # Search with wrong argument number (too high)
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("modify_wrong_arg_number")
            .receiver(DF_ADDR)
            .content([piaf.service.DFService.MODIFY_FUNC, "titi", "toto", "tata"])
            .build()
        )
        await queue_in.put(request)

        # Ensure response is a refuse
        response: Message = await queue_out.get()
        assert response.acl_message.performative == Performative.REFUSE
        assert response.acl_message.conversation_id == request.conversation_id
        assert response.acl_message.sender == DF_ADDR
        assert response.acl_message.content == [
            request.content,
            "Unexpected argument count",
        ]
        queue_out.task_done()

        # Modify a non-existing record
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("non-existing-record")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.MODIFY_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Agree because message is well formed
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == request.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == request.content

        # Failure because record doesn't exists
        failure: Message = await queue_out.get()

        assert failure.acl_message.performative == Performative.FAILURE
        assert failure.acl_message.conversation_id == request.conversation_id
        assert failure.acl_message.sender == DF_ADDR
        assert failure.acl_message.content == [request.content, "Not registered"]

        # Modify a record for another agent
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("other-agent")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.MODIFY_FUNC,
                    piaf.service.DFAgentDescription(AID("other_agent@localhost")),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Agree because message is well formed
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == request.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == request.content

        # Failure because unauthorized
        failure: Message = await queue_out.get()

        assert failure.acl_message.performative == Performative.FAILURE
        assert failure.acl_message.conversation_id == request.conversation_id
        assert failure.acl_message.sender == DF_ADDR
        assert failure.acl_message.content == [request.content, "Unauthorized"]

        # Create record
        register = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("register")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.REGISTER_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(register)

        # Agree and Inform
        _: Message = await queue_out.get()
        queue_out.task_done()
        _: Message = await queue_out.get()
        queue_out.task_done()

        # Modify an existing record
        request = (
            ACLMessage.Builder()
            .performative(Performative.REQUEST)
            .conversation_id("success")
            .receiver(DF_ADDR)
            .content(
                [
                    piaf.service.DFService.MODIFY_FUNC,
                    piaf.service.DFAgentDescription(helper_aid),
                ]
            )
            .build()
        )
        await queue_in.put(request)

        # Agree because message is well formed
        agree: Message = await queue_out.get()
        queue_out.task_done()

        assert agree.acl_message.conversation_id == request.conversation_id
        assert agree.acl_message.performative == Performative.AGREE
        assert agree.acl_message.sender == DF_ADDR
        assert agree.acl_message.content == request.content

        # Inform done
        done: Message = await queue_out.get()
        queue_out.task_done()

        assert done.acl_message.performative == Performative.INFORM
        assert done.acl_message.conversation_id == request.conversation_id
        assert done.acl_message.sender == DF_ADDR
        assert done.acl_message.content == request.content

        await ap.stop()
